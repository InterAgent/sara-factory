#!/bin/bash
set -e

psql -v ON_ERROR_STOP=1 --username "$POSTGRES_USER" --dbname "$POSTGRES_DB" <<-EOSQL
    CREATE USER sa100;
    ALTER USER sa100 WITH PASSWORD 'sa100';
    CREATE DATABASE sarafactory ;
    GRANT ALL PRIVILEGES ON DATABASE sarafactory TO sa100;
EOSQL
