'use strict';
$(document).ready(function() {
    // [ gridstack ] start
    $(function() {
        var options = {
            float: true,
            removable: '.trash',
            removeTimeout: 100,
            acceptWidgets: '.grid-stack-item'
        };
        $('.grid-stack').gridstack(options);
        new function() {
            this.items = [{
                    x: 4,
                    y: 4,
                    width: 2,
                    height: 2
                },
                {
                    x: 6,
                    y: 4,
                    width: 2,
                    height: 2
                }
            ];
            this.grid = $('.grid-stack').data('gridstack');
            this.addNewWidget = function() {
                var node = this.items.pop() || {
                    x: 4 + 12 * Math.random(),
                    y: 4 + 5 * Math.random(),
                    width: 2 + 3 * Math.random(),
                    height: 2 + 3 * Math.random()
                };
                this.grid.addWidget($('<div><div class="grid-stack-item-content" /><div class="grid-stack-item-content"> 資源排程 </div><div/>'),
                    node.x, node.y, node.width, node.height);
                return false;
            }.bind(this);
            $('#add-new-widget').click(this.addNewWidget);
        };
    });
    // [ gridstack ] end
});
