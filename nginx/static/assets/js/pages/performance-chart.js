function graphData(gd){
    var chart = AmCharts.makeChart("resource-loading", {
        "type": "serial",
        "theme": "light",
        "marginTop": 10,
        "marginRight": 0,
        "dataProvider": gd,
        "valueAxes": [{
            "axisAlpha": 0,
            "position": "left"
        }],
        "graphs": [{
        "id": "g1",
        "balloonText": "[[category]]<br><b><span style='font-size:14px;'>[[value]]</span></b>",
        "bullet": "round",
        "lineColor": "#10adf5",
        "lineThickness": 2,
        "negativeLineColor": "#10adf5",
        "valueField": "value",
        "title": "有效資源數",
        }, {
            "id": "g2",
            "balloonText": "[[category]]<br><b><span style='font-size:14px;'>[[value1]]</span></b>",
            "bullet": "round",
            "lineColor": "#1de9b6",
            "lineThickness": 2,
            "negativeLineColor": "#1de9b6",
            "valueField": "value1",
            "title": "負荷40%-80%",
        },{
            "id": "g3",
            "balloonText": "[[category]]<br><b><span style='font-size:14px;'>[[value2]]</span></b>",
            "bullet": "round",
            "lineColor": "#fdda08",
            "lineThickness": 2,
            "negativeLineColor": "#fdda08",
            "valueField": "value2",
            "title": "負荷<=40%",
        },{
            "id": "g4",
            "balloonText": "[[category]]<br><b><span style='font-size:14px;'>[[value3]]</span></b>",
            "bullet": "round",
            "lineColor": "#ff0000",
            "lineThickness": 2,
            "negativeLineColor": "#ff0000",
            "valueField": "value3",
            "title": "負荷>=80%",
        }],
    "chartCursor": {
        "cursorAlpha": 0,
        "valueLineEnabled": true,
        "valueLineBalloonEnabled": true,
        "valueLineAlpha": 0.3,
        "fullWidth": true
    },
    "categoryField": "day",
    "categoryAxis": {
        "minorGridAlpha": 0,
        "minorGridEnabled": true,
        "gridAlpha": 0,
        "axisAlpha": 0,
        "lineAlpha": 0
    },
    "legend": {
        "useGraphSettings": true,
        "position": "top",
        "labelText": "[[title]]",
        "fontSize": 14,
        "horizontalGap": 20
    },
});
}