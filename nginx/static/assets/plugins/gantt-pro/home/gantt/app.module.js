// import { Splitter, Gantt, ResourceHistogram, ProjectModel } from '/static/assets/plugins/gantt-pro/build/gantt.module.js';
import { Toolbar, Toast, DateHelper, Splitter, Gantt, Scheduler, ProjectModel } from '/static/assets/plugins/gantt-pro/build/gantt.module.js';

// import shared from '/static/assets/plugins/gantt-pro/home/_shared/shared.module.js';
/* eslint-disable no-unused-vars */

class GanttToolbar extends Toolbar {
    // Factoryable type name
    static get type() {
        return 'gantttoolbar';
    }

    static get $name() {
        return 'GanttToolbar';
    }

    // Called when toolbar is added to the Gantt panel
    set parent(parent) {
        super.parent = parent;

        const me = this;

        me.gantt = parent;

        parent.project.on({
            load    : 'updateStartDateField',
            refresh : 'updateStartDateField',
            thisObj : me
        });

        me.styleNode = document.createElement('style');
        document.head.appendChild(me.styleNode);
    }

    get parent() {
        return super.parent;
    }

    


    updateStartDateField() {
        const { startDateField } = this.widgetMap;

        // startDateField.value = this.gantt.project.startDate;

        // This handler is called on project.load/propagationComplete, so now we have the
        // initial start date. Prior to this time, the empty (default) value would be
        // flagged as invalid.
        // startDateField.required = true;
        this.gantt.zoomToFit({
            leftMargin  : 50,
            rightMargin : 50
        }); 
    
    }

    // region controller methods

    onEditTaskClick() {
        const { gantt } = this;

        if (gantt.selectedRecord) {
            gantt.editTask(gantt.selectedRecord);
        }
        else {
            Toast.show(this.L('First select the task you want to edit'));
        }
    }

    onZoomInClick() {
        this.gantt.zoomIn();
    }

    onZoomOutClick() {
        this.gantt.zoomOut();
    }

    onZoomToFitClick() {
        this.gantt.zoomToFit({
            leftMargin  : 50,
            rightMargin : 50
        });
    }

    onShiftPreviousClick() {
        this.gantt.shiftPrevious();
    }

    onShiftNextClick() {
        this.gantt.shiftNext();
    }

    onStartDateChange({ value, oldValue }) {
        if (!oldValue) { // ignore initial set
            return;
        }

        this.gantt.startDate = DateHelper.add(value, -1, 'week');

        this.gantt.project.setStartDate(value);
    }

    onFilterChange({ value }) {
        if (value === '') {
            this.gantt.taskStore.clearFilters();
        }
        else {
            value = value.replace(/[.*+?^${}()|[\]\\]/g, '\\$&');

            this.gantt.taskStore.filter({
                filters : task => task.name && task.name.match(new RegExp(value, 'i')),
                replace : true
            });
        }
    }

    // onFeaturesClick({ source : item }) {
    //     const { gantt } = this;

    //     if (item.feature) {
    //         const feature = gantt.features[item.feature];
    //         feature.disabled = !feature.disabled;
    //     }
    //     else if (item.subGrid) {
    //         const subGrid = gantt.subGrids[item.subGrid];
    //         subGrid.collapsed = !subGrid.collapsed;
    //     }
    // }

    // onFeaturesShow({ source : menu }) {
    //     const { gantt } = this;

    //     menu.items.map(item => {
    //         const { feature } = item;

    //         if (feature) {
    //             // a feature might be not presented in the gantt
    //             // (the code is shared between "advanced" and "php" demos which use a bit different set of features)
    //             if (gantt.features[feature]) {
    //                 item.checked = !gantt.features[feature].disabled;
    //             }
    //             // hide not existing features
    //             else {
    //                 item.hide();
    //             }
    //         }
    //         else {
    //             item.checked = gantt.subGrids[item.subGrid].collapsed;
    //         }
    //     });
    // }

    // endregion
};

// Register this widget type with its Factory
GanttToolbar.initClass();


// Project that will be shared by Gantt & SchedulerPro (Try use only Scheduler here)
const project = window.project = new ProjectModel({

    startDate : new Date(),
    endDate   : new Date(),

    // General calendar is used by default
    calendar : 'general',

    // transport : {
    //    load : {
    //        url : '/static/assets/plugins/gantt-pro/home/_datasets/m002.json'
    //    },

    transport : {
        load : {
            // url : 'http://192.168.99.101:80/api/load_gantt',
            url: 'http://' + document.domain + '/api/load_gantt',
            method : 'GET'
        },
        sync : {
            // url : 'http://192.168.99.101:80/api/sync_gantt',
            url : 'http://' + document.domain + '/api/sync_gantt',
        }
    },
    autoLoad : true,
    autoSync : true,
    autoSyncTimeout : 10000
});

const gantt = window.project = new Gantt({
    project,

    appendTo                : 'container-gantt',
    flex                    : 1,
    features : {
        baselines  : true,
        indicators : true,
        filter         : true,
        timeRanges     : {
            showCurrentTimeLine : true
        },
        labels : {
            left : {
                field  : 'name',
                editor : {
                    type : 'textfield'
                }
            }
        }
    },

    tbar : {
        type : 'gantttoolbar'
    },

    viewPreset  : 'weekAndDayLetter',
    columnLines : true,

    columns : [
        // { type : 'sequence', minWidth : 50, width : 50, text : '', align : 'right', resizable : false },
        { type : 'name', text: '工單排程', width : 280 },
        { type : 'resourceassignment', text : '排定資源', width : 160 }
    ],

    startDate : '2020-12-10T08:30:00',
    rowHeight : 50,
    barMargin : 15,


    listeners : {
        beforeCellEditStart : ({ editorContext }) => editorContext.column.field !== 'percentDone' || editorContext.record.isLeaf
    }
});

new Splitter({
    appendTo : 'container-gantt'
});

// const histogram = window.histogram = new ResourceHistogram({
//     appendTo    : 'container-gantt',
//     project,
//     hideHeaders : true,
//     partner     : gantt,
//     rowHeight   : 50,
//     showBarTip  : true,
//     columns     : [
//         { field : 'name',
//           text  : '資源負荷', 
//           width : 400  }
//     ]
// });

const scheduler = new Scheduler({
    project,

    appendTo            : 'container-gantt',
    minHeight           : '20em',
    flex                : 1,
    partner             : gantt,
    rowHeight           : 50,
    barMargin           : 15,
    eventColor          : 'gantt-green',

    allowOverlap      : true,

    columns : [
        // { type : 'sequence', minWidth : 50, width : 50, text : '', align : 'right', resizable : false },

        {
            type           : 'resourceInfo',
            field          : 'name',
            text           : '資源排程',
            showEventCount : true,
            // showRole       : true,
            showImage      : false,
            width          : 280
        },
        {
            type  : 'resourceCalendar',
            text  : '工作時段',
            width : 160
        }
    ]
});
