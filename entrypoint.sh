#!/bin/bash
nohup gunicorn -b "0.0.0.0:8000" --workers=9 --timeout=10000 --capture-output --log-level=info "run:app" &
celery worker -l info --concurrency=1 -A app.tasks &
celery -A app.tasks:celery beat --loglevel=INFO --pidfile=/celeryd.pid