# -*- encoding: utf-8 -*-
"""
License: Commercial
Copyright (c) 2019 - present AppSeed.us
"""
from datetime import datetime
from flask_login import UserMixin
from sqlalchemy import Binary, Column, Integer, String, DateTime
from sqlalchemy.ext.mutable import Mutable
from sqlalchemy.dialects.postgresql import ARRAY

from sqlalchemy.types import TypeDecorator
from sqlalchemy.dialects.postgresql import JSON
import json
from app import db, login_manager
from app.base.util import hash_pass
from app.api.utils import get_current_time_string

@login_manager.user_loader
def user_loader(id):
    return User.query.filter_by(id=id).first()

@login_manager.request_loader
def request_loader(request):
    username = request.form.get('username')
    user = User.query.filter_by(username=username).first()
    return user if user else None


class MutableList(Mutable, list):
    def append(self, value):
        list.append(self, value)
        self.changed()
        
    def pop(self, index=0):
        value = list.pop(self, index)
        self.changed()
        return value

    @classmethod
    def coerce(cls, key, value):
        if not isinstance(value, MutableList):
            if isinstance(value, list):
                return MutableList(value)
            return Mutable.coerce(key, value)
        else:
            return value

class User(db.Model, UserMixin):
    __tablename__ = 'users'

    id = Column(Integer, primary_key=True)
    username = Column(String, unique=True)
    email = Column(String, unique=True)
    password = Column(Binary)
    role = Column(String)
    last_seen = Column(DateTime, default = datetime.utcnow)

    def __init__(self, **kwargs):
        for property, value in kwargs.items():
            # depending on whether value is an iterable or not, we must
            # unpack it's value (when **kwargs is request.form, some values
            # will be a 1-element list)
            if hasattr(value, '__iter__') and not isinstance(value, str):
                # the ,= unpack of a singleton fails PEP8 (travis flake8 test)
                value = value[0]

            if property == 'password':
                value = hash_pass( value ) # we need bytes here (not plain str)
                
            setattr(self, property, value)

    def __repr__(self):
        return str(self.username)

class SQLBottleneckSetting(db.Model):
    __tablename__ = 'bottleneck_setting_db'
    id = db.Column(db.Integer, primary_key=True)
    index = db.Column(db.String)
    job_name = db.Column(db.String)
    ignore = db.Column(db.String,default="never")
    warning = db.Column(db.Integer,default=4)
    urgent = db.Column(db.Integer,default=20)

class SQLNotificationModel(db.Model):
    __tablename__ = 'notification_db'
    id = db.Column(db.Integer, primary_key=True)
    index = db.Column(db.String)
    jid = db.Column(db.String)
    rid = db.Column(db.String)
    event_type = db.Column(db.String)
    start_time = db.Column(db.String) #回報
    end_time = db.Column(db.String) #排除
    report_time = db.Column(db.String) #回報重排
    end_received_time = db.Column(db.String) #排除重排
    status = db.Column(db.String)
    type = db.Column(db.String)

class SQLErrorSetting(db.Model):
    __tablename__ = 'error_setting_db'
    id = db.Column(db.Integer, primary_key=True)
    index = db.Column(db.String)
    error_code = db.Column(db.String)
    error_type = db.Column(db.String)
    error_group = db.Column(db.String)
    error_detail = db.Column(db.String)


class SQLSchedulerSetting(db.Model):
    __tablename__ = 'scheduler_setting_db'
    id = db.Column(db.Integer, primary_key=True)
    created_time = db.Column(db.String)

    reschedule_timer = db.Column(db.String)
    ml_model = db.Column(db.String)
    sequencing_method = db.Column(db.String)
    dispatching_method = db.Column(db.Boolean)

    scheduler_version = db.Column(MutableList.as_mutable(ARRAY(db.String)),default=[])
    
class SQLScheduleDB(db.Model):
    __tablename__ = 'schedule_db'
    id = db.Column(db.Integer, primary_key=True)
    index = db.Column(db.String)
    time = db.Column(db.String)
    schedule_result = db.Column(db.JSON)
    schedule_status = db.Column(db.String)
    gantt_result = db.Column(db.JSON)
    gantt_status = db.Column(db.String)
    status = db.Column(db.String)
    schedule_summary = db.Column(db.JSON) 
    selected = db.Column(db.Boolean,default=True)

class SQLProjectModel(db.Model):
    __tablename__ = 'project_db'
    
    id = db.Column(db.Integer, primary_key=True)
    index = db.Column(db.String)
    order_id = db.Column(db.String) #工單編號
    order_date = db.Column(db.String) 
    project_name = db.Column(db.String) #製品名稱
    project_type = db.Column(db.String,default='一般') #急單
    due = db.Column(db.String)
    customer_id = db.Column(db.String)

    plan_end_time = db.Column(db.String)
    _product = db.Column(MutableList.as_mutable(ARRAY(db.String)),default=[])
    status = db.Column(db.String,default="New")
    updated_on = db.Column(db.String, default=get_current_time_string())
    abnormal_status = db.Column(db.JSON, default=dict(default=False)) 

    def __json__(self):
        return ["order_id", "order_date", "project_name", "project_type", "due", "_product", "customer_id", "status", "updated_on"]
    def keys(self):
        return ["order_id","order_date","project_name","project_type","due","customer_id"]
        
class SQLProductModel(db.Model):
    __tablename__ = 'product_db'
    id = db.Column(db.Integer, primary_key=True)
    index = db.Column(db.String)
    project_name = db.Column(db.String)
    product_name = db.Column(db.String)
    lot_nbr = db.Column(db.String)
    mo_nbr_step = db.Column(db.String) 
    
    project_id = db.Column(db.String)
    _jobID_list = db.Column(db.JSON,default={})
    _process = db.Column(MutableList.as_mutable(ARRAY(db.String)),default=[])
    plan_end_time = db.Column(db.String)
    status = db.Column(db.String,default="New")
    mother = db.Column(db.JSON,default={})
    current_loc = db.Column(db.String)
    order_date = db.Column(db.String)
    due = db.Column(db.String)
    order_id = db.Column(db.String)
    product_full_name = db.Column(db.String)
    
    def __json__(self):
        return ["project_id", "product_name", "_jobID_list", "_process", "mother" ]
    def keys(self):
        return ["project_name","product_name","lot_nbr"]


class SQLJobLib(db.Model):
    __tablename__ = "job_library"
    id = db.Column(db.Integer, primary_key=True)
    index        = db.Column(db.String) 
    job_name     = db.Column(db.String)
    jlb_name     = db.Column(db.String)
    route_seq    = db.Column(db.String)
    process_name = db.Column(db.String)
    primary_resource = db.Column(db.JSON)
    secondary_resource = db.Column(db.JSON)
    _material     = db.Column(db.JSON)
    full_name = db.Column(db.String)
    skill        = db.Column(db.Integer,default="9")
    sourcing = db.Column(db.String,default="in-house")
    jlb_rule = db.Column(db.String)
    

    def __json__(self):
        return ["index", "job_name","process_name", \
                "primary_resource", "secondary_resource","sourcing",\
                "_material","skill"]

    def keys(self):
        return ["process_name","jlb_name","sourcing","_material","full_name", "job_name","route_seq"]


class SQLJobModel(db.Model):
    __tablename__ = 'job_db'
    id = db.Column(db.Integer, primary_key=True)
    project_id   = db.Column(db.String)
    project_name = db.Column(db.String)
    product_id   = db.Column(db.String)
    product_name = db.Column(db.String)
    index        = db.Column(db.String)
    qty          = db.Column(db.Integer)
    job_sequence = db.Column(db.Integer)
    route_seq = db.Column(db.String)
    est_time     = db.Column(db.Integer)
    pre_product  = db.Column(db.String)
    real_start_time = db.Column(db.String)
    real_end_time = db.Column(db.String)
    plan_start_time = db.Column(db.String)
    plan_end_time = db.Column(db.String)
    mother = db.Column(db.JSON,default={})
    status = db.Column(db.String,default="New")
    resource = db.Column(db.JSON,default={})
    history = db.Column(MutableList.as_mutable(ARRAY(db.JSON)),default=[])

    abnormalID = db.Column(db.String)
    abnormal_status = db.Column(db.JSON, default=dict(
        operation_delay=0,working_hours_mismatch=False,abnormal_sequence=False,abnormal_report=0
    ))

    plan_working_time = db.Column(db.Integer,default=0)
    working_time_by_day = db.Column(db.JSON,default={})
    bottleneck = db.Column(db.Boolean, default=False)
    waiting_time = db.Column(db.Float)
    lock = db.Column(db.Boolean, default=False)
    good_qty = db.Column(db.Integer,default=0)
    bad_qty = db.Column(db.Integer,default=0)

    job_name     = db.Column(db.String)
    detail = db.Column(db.JSON)
    translated_detail = db.Column(MutableList.as_mutable(ARRAY(db.JSON)),default=[])
    job_parameter = db.Column(db.Integer)
    
    # process_name = db.Column(db.String)
    # primary_resource = db.Column(db.JSON)
    # secondary_resource = db.Column(db.JSON)
    # sourcing = db.Column(db.String)
    # _material     = db.Column(db.JSON)
    # skill        = db.Column(db.Integer)

    jlb_id = Column(db.Integer, db.ForeignKey('job_library.id'))
    jlb = db.relationship("SQLJobLib", backref="SQLJobModel")
    prd_id = Column(db.Integer, db.ForeignKey('product_db.id'))
    #product_id2 = Column(db.Integer, db.ForeignKey('product_db.index'))
    product = db.relationship("SQLProductModel", backref="SQLJobModel")
    # rid = Column(db.Integer, db.ForeignKey('resource_db.id'))
    # sql_resource = db.relationship("SQLResourceModel", backref="SQLJobModel")


    # buffer_time = db.Column(db.Float)
    def keys(self):
        return ["project_name","product_name","qty","job_sequence","job_name", \
            "process_name","route_seq","est_time","sourcing", "jlb_name","job_parameter"]
    def __json__(self):
        return ["project_name","project_id","product_name",\
                "product_id", "job_name", "qty", "job_sequence", "est_time", "pre_product", "mother","jlb_id","detail","translated_detail","job_parameter"]
    def __time__(self):

        return ["project_name","project_id","product_name","resource",\
                "product_id", "job_name", "job_sequence", "qty","est_time", "mother","jlb_id",
                "plan_start_time", "plan_end_time", "real_start_time","real_end_time","waiting_time","bottleneck", "status","detail","translated_detail","job_parameter"]
class SQLProcessModel(db.Model):
    __tablename__ = 'process_db'
    id = db.Column(db.Integer, primary_key=True)
    index = db.Column(db.String)
    process_name = db.Column(db.String)
    _job = db.Column(MutableList.as_mutable(ARRAY(db.String)),default=[])

class SQLWorkingDays(db.Model):
    __tablename__ = 'workingdays_db'
    id = db.Column(db.Integer,primary_key=True)
    index = db.Column(db.String)
    calendar_name = db.Column(db.String)
    working_days = db.Column(MutableList.as_mutable(ARRAY(db.String)),default=[])


class SQLWorkingHours(db.Model):
    __tablename__ = 'workinghours_db'
    id = db.Column(db.Integer,primary_key=True)
    index = db.Column(db.String)
    hours_name = db.Column(db.String)

    working_hours = db.Column(MutableList.as_mutable(ARRAY(db.String)),default=[])
    break_time = db.Column(MutableList.as_mutable(ARRAY(db.String)),default=[])

class SQLMaterialModel(db.Model):
    __tablename__ = 'material_db'
    id = db.Column(db.Integer,primary_key=True)
    material_name = db.Column(db.String)
    unit= db.Column(db.String)
    material_type = db.Column(db.String)
    material_describtion = db.Column(db.String)

    min_inventory = db.Column(db.Integer,default=0)
    inventory= db.Column(db.Integer,default=0)
    available = db.Column(db.Integer,default=0)
    lead_time =  db.Column(db.Integer,default=0)

class SQLResourceModel(db.Model):
    __tablename__ = 'resource_db'

    # personal info
    id = db.Column(db.Integer,primary_key=True)
    index = db.Column(db.String)
    resource_name = db.Column(db.String)
    resource_type = db.Column(db.String,default="machine")
    job_name = db.Column(MutableList.as_mutable(ARRAY(db.String)),default=[])
    virtual_resource = db.Column(db.Boolean, default=False)
    working_day = db.Column(db.String,default="WDID0")
    working_hours = db.Column(db.String,default="WHID0")

    wdid = Column(db.Integer, db.ForeignKey('workingdays_db.id'))
    whid = Column(db.Integer, db.ForeignKey('workinghours_db.id'))
    workingDays = db.relationship("SQLWorkingDays", backref="SQLResourceModel")
    workingHours = db.relationship("SQLWorkingHours", backref="SQLResourceModel")

    operator_skill = db.Column(db.Integer,default=9)
    sourcing = db.Column(db.String,default="in-house")

    # unused variable
    working_type = db.Column(db.String)
    working_capacity = db.Column(db.Integer)
    working_flow_gap = db.Column(db.Integer)    # in seconds
    working_flow_time = db.Column(db.Integer)    # in seconds

    # scheduler setting
    in_between = db.Column(db.Boolean,default=True)
    allow_overlapped = db.Column(db.Boolean,default=False)
    capacity = db.Column(db.Integer,default=1)
    lock_time = db.Column(db.Integer, default=0)

    # checker
    status = db.Column(db.String,default="standBy")
    jid_list = db.Column(MutableList.as_mutable(ARRAY(db.String)),default=[])
    history = db.Column(MutableList.as_mutable(ARRAY(db.JSON)),default=[])
    abnormalID = db.Column(db.String)
    abnormal_status = db.Column(db.JSON,default=dict(abnormal_report=0))

    day_off = db.Column(db.JSON, default={})
    day_on = db.Column(db.JSON, default={})
    lock_time_start = db.Column(db.String)
    lock_time_end = db.Column(db.String)

    def __json__(self):
        return ["resource_name", "job_name", "resource_type", "working_type", "operator_skill", "working_day", "working_hours", "virtual_resource", "allow_overlapped", "capacity", "in_between"]

