# -*- encoding: utf-8 -*-
from datetime import datetime
from flask import jsonify, render_template, redirect, request, url_for, g
from flask_login import (
    current_user,
    login_required,
    login_user,
    logout_user
)
# from flask_paginate import Pagination, get_page_args

from app import db, login_manager
from app.base import blueprint
from app.base.forms import LoginForm, CreateAccountForm
from app.base.models import User
from app.base.util import verify_pass
import requests

@blueprint.before_app_request
def before_quest():
    if current_user.is_authenticated:
        current_user.last_seen = datetime.utcnow()
        db.session.commit()

@blueprint.route('/')
def route_default():

    #username = request.cookies.get('username')
    #print("DEBUG:{}".format(username))
    #if username == None:
    #    return render_tempate('base_blueprint.login')

    #user = User.query.filter_by(username=username).first()
    #print("DEBUG ROLE:{} Enter {}".format(user.role,type(user.role)))
            
    #if user.role == 'admin':
    #    return redirect(url_for('home_blueprint.index'))
    #elif user.role == 'limited_access':
    #    return redirect(url_for('home_blueprint.index_user'))
    return redirect(url_for('base_blueprint.login'))

@blueprint.route('/error-<error>')
def route_errors(error):
    return render_template('errors/{}.html'.format(error))

## Login & Registration

@blueprint.route('/login', methods=['GET', 'POST'])
def login():
    login_form = LoginForm(request.form)
    if 'login' in request.form:
        
        # read form data
        username = request.form['username']
        password = request.form['password']

        # Locate user
        user = User.query.filter_by(username=username).first()
        
        # Check the password
        if user and verify_pass( password, user.password):

            login_user(user)
            print("DEBUG MODE: ENTER AGAIN TO INDEX PAGE")
            return redirect(url_for('base_blueprint.route_default'))

        # Something (user or pass) is not ok
        return render_template( 'accounts/auth-signin.html', msg='帳號密碼錯誤，請重新登入', error='raise', form=login_form)

    if not current_user.is_authenticated:
        return render_template( 'accounts/auth-signin.html', form=login_form)
    #return redirect(url_for('home_blueprint.index'))
    if current_user.role == 'admin':
        
        return redirect(url_for('home_blueprint.index'))
    else:
        return redirect(url_for('home_blueprint.work_center'))

    
    
@blueprint.route('/register', methods=['GET', 'POST'])
def register():
    login_form = LoginForm(request.form)
    create_account_form = CreateAccountForm(request.form)
    if 'register' in request.form:

        username  = request.form['username']
        email     = request.form['email'   ]
        #role      = request.form['role']
        #role      = 'limited_access'
        role      = 'admin' 
        #role      = 'sales'
        #Joy: if role is an admin, there should be another password.
        
        user = User.query.filter_by(username=username).first()
        if user:
            return render_template( 'accounts/register.html', msg='Username already registered', form=create_account_form)

        user = User.query.filter_by(email=email).first()
        if user:
            return render_template( 'accounts/register.html', msg='Email already registered', form=create_account_form)

        # else we can create the user
        user = User(**request.form, role = role)
        db.session.add(user)
        db.session.commit()

        return render_template( 'accounts/register.html', msg='User created successfully!', form=create_account_form)

    else:
        return render_template( 'accounts/register.html', form=create_account_form)

        
@blueprint.route('/reset_password', methods=['GET', 'POST'])
def reset_password():

    pass

    return render_template( 'accounts/auth-reset-password.html')

@blueprint.route('/gantt')
@login_required
def gantt():

    
    return render_template( '/gantt/index.html' )

@blueprint.route('/logout')
def logout():
    logout_user()
    return redirect(url_for('base_blueprint.login'))

@blueprint.route('/shutdown')
def shutdown():
    func = request.environ.get('werkzeug.server.shutdown')
    if func is None:
        raise RuntimeError('Not running with the Werkzeug Server')
    func()
    return 'Server shutting down...'

# @blueprint.route('/user-list', defaults={'page': 1})
@blueprint.route('/user-list')
@login_required
def user_list():

    # paginated_users = User.query.all()

    paginated_users = User.query \
        .order_by(User.last_seen.desc()) \
        .paginate()
        

    return render_template('users/user_list.html',
                           users=paginated_users)

## Errors

@login_manager.unauthorized_handler
def unauthorized_handler():
    return render_template('errors/403.html'), 403

@blueprint.errorhandler(403)
def access_forbidden(error):
    return render_template('errors/403.html'), 403

@blueprint.errorhandler(404)
def not_found_error(error):
    return render_template('errors/404.html'), 404

@blueprint.errorhandler(500)
def internal_error(error):
    return render_template('errors/500.html'), 500
 