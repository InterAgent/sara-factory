# -*- encoding: utf-8 -*-
"""
License: Commercial
Copyright (c) 2019 - present AppSeed.us
"""
from datetime import datetime
from flask_login import UserMixin
from sqlalchemy import Binary, Column, Integer, String, DateTime
from sqlalchemy.ext.mutable import Mutable
from sqlalchemy.dialects.postgresql import ARRAY

from sqlalchemy.types import TypeDecorator
from sqlalchemy.dialects.postgresql import JSON
import json
from app import db, login_manager
from app.base.util import hash_pass
from app.api.utils import get_current_time_string


class MutableList(Mutable, list):
    def append(self, value):
        list.append(self, value)
        self.changed()
        
    def pop(self, index=0):
        value = list.pop(self, index)
        self.changed()
        return value

    @classmethod
    def coerce(cls, key, value):
        if not isinstance(value, MutableList):
            if isinstance(value, list):
                return MutableList(value)
            return Mutable.coerce(key, value)
        else:
            return value

class p_SQLProjectModel(db.Model):
    __tablename__ = 'p_project_db'
    
    id = db.Column(db.Integer, primary_key=True)
    index = db.Column(db.String)
    order_id = db.Column(db.String) #工單編號
    order_date = db.Column(db.String) 
    project_name = db.Column(db.String) #製品名稱
    project_type = db.Column(db.String,default='一般') #急單
    due = db.Column(db.String)
    customer_id = db.Column(db.String)

    plan_end_time = db.Column(db.String)
    _product = db.Column(MutableList.as_mutable(ARRAY(db.String)),default=[])
    status = db.Column(db.String,default="New")
    updated_on = db.Column(db.String, default=get_current_time_string())
    abnormal_status = db.Column(db.JSON, default=dict(default=False)) 

    def __json__(self):
        return ["order_id", "order_date", "project_name", "project_type", "due", "_product", "customer_id", "status", "updated_on"]
    def keys(self):
        return ["order_id","order_date","project_name","project_type","due","customer_id"]
        
class p_SQLProductModel(db.Model):
    __tablename__ = 'p_product_db'
    id = db.Column(db.Integer, primary_key=True)
    index = db.Column(db.String)
    project_name = db.Column(db.String)
    product_name = db.Column(db.String)
    lot_nbr = db.Column(db.String)
    mo_nbr_step = db.Column(db.String) 
    
    project_id = db.Column(db.String)
    _jobID_list = db.Column(db.JSON,default={})
    _process = db.Column(MutableList.as_mutable(ARRAY(db.String)),default=[])
    plan_end_time = db.Column(db.String)
    status = db.Column(db.String,default="New")
    mother = db.Column(db.JSON,default={})
    current_loc = db.Column(db.String)
    order_date = db.Column(db.String)
    due = db.Column(db.String)
    order_id = db.Column(db.String)
    product_full_name = db.Column(db.String)
    
    def __json__(self):
        return ["project_id", "product_name", "_jobID_list", "_process", "mother" ]
    def keys(self):
        return ["project_name","product_name","lot_nbr"]


class p_SQLJobLib(db.Model):
    __tablename__ = "p_job_library"
    id = db.Column(db.Integer, primary_key=True)
    index        = db.Column(db.String) 
    job_name     = db.Column(db.String)
    jlb_name     = db.Column(db.String)
    route_seq    = db.Column(db.String)
    process_name = db.Column(db.String)
    primary_resource = db.Column(db.JSON)
    secondary_resource = db.Column(db.JSON)
    _material     = db.Column(db.JSON)
    full_name = db.Column(db.String)
    skill        = db.Column(db.Integer,default="9")
    sourcing = db.Column(db.String,default="in-house")
    jlb_rule = db.Column(db.String)
    

    def __json__(self):
        return ["index", "job_name","process_name", \
                "primary_resource", "secondary_resource","sourcing",\
                "_material","skill"]

    def keys(self):
        return ["process_name","jlb_name","sourcing","_material","full_name", "job_name","route_seq"]


class p_SQLJobModel(db.Model):
    __tablename__ = 'p_job_db'
    id = db.Column(db.Integer, primary_key=True)
    project_id   = db.Column(db.String)
    project_name = db.Column(db.String)
    product_id   = db.Column(db.String)
    product_name = db.Column(db.String)
    index        = db.Column(db.String)
    qty          = db.Column(db.Integer)
    job_sequence = db.Column(db.Integer)
    route_seq = db.Column(db.String)
    est_time     = db.Column(db.Integer)
    pre_product  = db.Column(db.String)
    real_start_time = db.Column(db.String)
    real_end_time = db.Column(db.String)
    plan_start_time = db.Column(db.String)
    plan_end_time = db.Column(db.String)
    mother = db.Column(db.JSON,default={})
    status = db.Column(db.String,default="New")
    resource = db.Column(db.JSON,default={})
    history = db.Column(MutableList.as_mutable(ARRAY(db.JSON)),default=[])

    abnormalID = db.Column(db.String)
    abnormal_status = db.Column(db.JSON, default=dict(
        operation_delay=0,working_hours_mismatch=False,abnormal_sequence=False,abnormal_report=0
    ))

    plan_working_time = db.Column(db.Integer,default=0)
    working_time_by_day = db.Column(db.JSON,default={})
    bottleneck = db.Column(db.Boolean, default=False)
    waiting_time = db.Column(db.Float)
    lock = db.Column(db.Boolean, default=False)
    good_qty = db.Column(db.Integer,default=0)
    bad_qty = db.Column(db.Integer,default=0)

    job_name     = db.Column(db.String)
    detail = db.Column(db.JSON)
    translated_detail = db.Column(MutableList.as_mutable(ARRAY(db.JSON)),default=[])
    job_parameter = db.Column(db.Integer)
    
    # process_name = db.Column(db.String)
    # primary_resource = db.Column(db.JSON)
    # secondary_resource = db.Column(db.JSON)
    # sourcing = db.Column(db.String)
    # _material     = db.Column(db.JSON)
    # skill        = db.Column(db.Integer)

    jlb_id = Column(db.Integer, db.ForeignKey('p_job_library.id'))
    jlb = db.relationship("p_SQLJobLib", backref="p_SQLJobModel")
    prd_id = Column(db.Integer, db.ForeignKey('p_product_db.id'))
    #product_id2 = Column(db.Integer, db.ForeignKey('product_db.index'))
    product = db.relationship("p_SQLProductModel", backref="p_SQLJobModel")
    # rid = Column(db.Integer, db.ForeignKey('resource_db.id'))
    # sql_resource = db.relationship("SQLResourceModel", backref="SQLJobModel")


    # buffer_time = db.Column(db.Float)
    def keys(self):
        return ["project_name","product_name","qty","job_sequence","job_name", \
            "process_name","route_seq","est_time","sourcing", "jlb_name","job_parameter"]
    def __json__(self):
        return ["project_name","project_id","product_name",\
                "product_id", "job_name", "qty", "job_sequence", "est_time", "pre_product", "mother","jlb_id","detail","translated_detail","job_parameter"]
    def __time__(self):

        return ["project_name","project_id","product_name","resource",\
                "product_id", "job_name", "job_sequence", "qty","est_time", "mother","jlb_id",
                "plan_start_time", "plan_end_time", "real_start_time","real_end_time","waiting_time","bottleneck", "status","detail","translated_detail","job_parameter"]

class p_SQLProcessModel(db.Model):
    __tablename__ = 'p_process_db'
    id = db.Column(db.Integer, primary_key=True)
    index = db.Column(db.String)
    process_name = db.Column(db.String)
    _job = db.Column(MutableList.as_mutable(ARRAY(db.String)),default=[])

class p_SQLWorkingDays(db.Model):
    __tablename__ = 'p_workingdays_db'
    id = db.Column(db.Integer,primary_key=True)
    index = db.Column(db.String)
    calendar_name = db.Column(db.String)
    working_days = db.Column(MutableList.as_mutable(ARRAY(db.String)),default=[])


class p_SQLWorkingHours(db.Model):
    __tablename__ = 'p_workinghours_db'
    id = db.Column(db.Integer,primary_key=True)
    index = db.Column(db.String)
    hours_name = db.Column(db.String)

    working_hours = db.Column(MutableList.as_mutable(ARRAY(db.String)),default=[])
    break_time = db.Column(MutableList.as_mutable(ARRAY(db.String)),default=[])

class p_SQLMaterialModel(db.Model):
    __tablename__ = 'p_material_db'
    id = db.Column(db.Integer,primary_key=True)
    material_name = db.Column(db.String)
    unit= db.Column(db.String)
    material_type = db.Column(db.String)
    material_describtion = db.Column(db.String)

    min_inventory = db.Column(db.Integer,default=0)
    inventory= db.Column(db.Integer,default=0)
    available = db.Column(db.Integer,default=0)
    lead_time =  db.Column(db.Integer,default=0)

class p_SQLResourceModel(db.Model):
    __tablename__ = 'p_resource_db'

    # personal info
    id = db.Column(db.Integer,primary_key=True)
    index = db.Column(db.String)
    resource_name = db.Column(db.String)
    resource_type = db.Column(db.String,default="machine")
    job_name = db.Column(MutableList.as_mutable(ARRAY(db.String)),default=[])
    virtual_resource = db.Column(db.Boolean, default=False)
    working_day = db.Column(db.String,default="WDID0")
    working_hours = db.Column(db.String,default="WHID0")

    wdid = Column(db.Integer, db.ForeignKey('p_workingdays_db.id'))
    whid = Column(db.Integer, db.ForeignKey('p_workinghours_db.id'))
    workingDays = db.relationship("p_SQLWorkingDays", backref="p_SQLResourceModel")
    workingHours = db.relationship("p_SQLWorkingHours", backref="p_SQLResourceModel")

    operator_skill = db.Column(db.Integer,default=9)
    sourcing = db.Column(db.String,default="in-house")

    # unused variable
    working_type = db.Column(db.String)
    working_capacity = db.Column(db.Integer)
    working_flow_gap = db.Column(db.Integer)    # in seconds
    working_flow_time = db.Column(db.Integer)    # in seconds

    # scheduler setting
    in_between = db.Column(db.Boolean,default=True)
    allow_overlapped = db.Column(db.Boolean,default=False)
    capacity = db.Column(db.Integer,default=1)
    lock_time = db.Column(db.Integer, default=0)

    # checker
    status = db.Column(db.String,default="standBy")
    jid_list = db.Column(MutableList.as_mutable(ARRAY(db.String)),default=[])
    history = db.Column(MutableList.as_mutable(ARRAY(db.JSON)),default=[])
    abnormalID = db.Column(db.String)
    abnormal_status = db.Column(db.JSON,default=dict(abnormal_report=0))

    day_off = db.Column(db.JSON, default={})
    day_on = db.Column(db.JSON, default={})
    lock_time_start = db.Column(db.String)
    lock_time_end = db.Column(db.String)

    def __json__(self):
        return ["resource_name", "job_name", "resource_type", "working_type", "operator_skill", "working_day", "working_hours", "virtual_resource", "allow_overlapped", "capacity", "in_between"]
