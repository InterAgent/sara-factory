
CUSTOMER_NAME = "spcb"
CUSTOMER_PORT = "http://122.147.134.149"
CUSTOMER_MES_API = CUSTOMER_PORT + "/api/v1/schedule_result"
CUSTOMER_MES_RESULT_API = CUSTOMER_PORT + "/api/v1/mes_result" # 3 arguments / project_name/ product_id / lot_nbr
CUSTOMER_DB_NAME = "sarafactory"
CUSTOMER_ERP_INPR_ORDER_API  = CUSTOMER_PORT + "/api/v1/erp_inpr_order"
CUSTOMER_ERP_INPR_ORDER_DETAIL_API = CUSTOMER_PORT + "/api/v1/erp_inpr_order_detail"