import os
import json
import time

import pandas as pd

from app.api.utils import save_json_to_file
from app.datagent.utils import get_ver_config, translate, json_to_df, excel_to_df, map_keys, cut_time, _clean,trim, get_sheetnames_xlsx
from app.datagent import wip


def create_inventory(mode,ver_config,path=None,df=None,customer_name=None):
    from app.datagent import inventory
    if mode == "pipeline":
        template_path = os.path.join('app','data','excel_template.xlsx')
        if "calendar_name" not in df.columns:
            calendar_df = excel_to_df(template_path,sheet_name="workingdays")
                 
            inventory.make_calendar_inventory(
                translate(calendar_df),
                ver_config['workingdays'])
        else:
            inventory.make_calendar_inventory(df,ver_config['workingdays'])

        if "hours_name" not in df.columns:
            shift_df = excel_to_df(template_path,sheet_name="workinghours")
            inventory.make_shift_inventory(
                translate(shift_df),
                ver_config['workinghours'])
        else:
            inventory.make_shift_inventory(df,ver_config['workinghours'])

        inventory.make_process_inventory(df,ver_config['process'])
        inventory.make_resource_inventory(df,ver_config['resource'])
        inventory.make_jlb_inventory(df,ver_config['jlb'])
    else:
        sheet_name = get_sheetnames_xlsx(path) 
        if "workingdays" in sheet_name:
            inventory.make_calendar_inventory(
                translate(excel_to_df(path,sheet_name="workingdays"),mapper=None, customer_name=customer_name),
                ver_config['workingdays'])
        else:
            template_path = os.path.join('app','data','excel_template.xlsx')
            calendar_df = excel_to_df(template_path,sheet_name="workingdays")
        
            inventory.make_calendar_inventory(
                translate(calendar_df),
                ver_config['workingdays'])
        if "workinghours" in sheet_name:
            inventory.make_shift_inventory(
                translate(excel_to_df(path,sheet_name="workinghours"),mapper=None, customer_name=customer_name),
                ver_config['workinghours'])
        else:
            template_path = os.path.join('app','data','excel_template.xlsx')

            shift_df = excel_to_df(template_path,sheet_name="workinghours")
            inventory.make_shift_inventory(
                translate(shift_df),
                ver_config['workinghours'])
        if "material_data" in sheet_name:
            inventory.make_material_inventory(
                translate(excel_to_df(path,sheet_name="material_data"),mapper=None, customer_name=customer_name),
                ver_config['material'])
        if "project_data" in sheet_name:
            inventory.make_process_inventory(
                translate(excel_to_df(path,sheet_name="project_data"),mapper=None, customer_name=customer_name),
                ver_config['process'])
        if "resource_data" in sheet_name:
            print("==== resource_data defined === ")
            inventory.make_resource_inventory(
                translate(excel_to_df(path,sheet_name="resource_data"),mapper=None, customer_name=customer_name),
                ver_config['resource'])
        else:
            print("==== resource is not defined ====")
            inventory.make_resource_inventory(
                translate(excel_to_df(path,sheet_name="project_data"),mapper=None, customer_name=customer_name),
                ver_config['resource']) 

        if "outsourcing_data" in sheet_name:
            inventory.make_oursourcing_inventory(
                translate(excel_to_df(path,sheet_name="outsourcing_data"),mapper=None, customer_name=customer_name),
                ver_config['outsourcing'])
        if "project_data" in sheet_name:
            inventory.make_jlb_inventory(
                translate(excel_to_df(path,sheet_name="project_data"),mapper=None, customer_name=customer_name),
                ver_config['jlb'])

def mes_update_wip(customer_name):
    
    ver_config = get_ver_config()
    doc = wip.get_MES_update_result()
    path = os.path.join('app','data',"mes_update_result_0428.json")
    save_json_to_file(doc,filename="",path=path)
    df = _clean(pd.DataFrame.from_dict(doc))
    customer_sys_mapper, reversed_map = map_keys(list(df.columns.values),customer_name+"MES")
    df = translate(df, customer_sys_mapper)

    
    wip.job_MES_update(
        trim(
            df,
            keys = ['plan_start_time', 'plan_end_time','real_start_time','real_end_time', 
                   'project_name','product_name','lot_nbr','route_seq',"process_name","job_name"],
            subset=None
        ))
    return doc

def switcher(mode,path=None,customer_name=None):
    

    ver_config = get_ver_config()
    df = None
    if mode == "pipeline":
        erp_df = json_to_df(path[0])
        

        customer_sys_mapper, reversed_map = map_keys(list(erp_df.columns.values),customer_name)
        
        erp_df = translate(erp_df, customer_sys_mapper)
         
        mes_df = json_to_df(path[1])
        customer_sys_mapper, reversed_map = map_keys(list(mes_df.columns.values),customer_name)
        mes_df = translate(mes_df, customer_sys_mapper)
        # 重新盤點
        create_inventory(mode=mode,ver_config=ver_config, df=mes_df)
 
        #mes_df = cut_time(mes_df,n=32)
        
        # 新增wip

        uniq_projects, uniq_products = wip.create_wip(erp_df,ver_config, mode=mode)
        
        # 更新id
        if "lot_nbr" not in mes_df.columns:
            mes_df['lot_nbr'] = '001'
        mes_df["product_full_name"] = mes_df["project_name"] + '_' + mes_df["product_name"] + '_' + mes_df['lot_nbr']
        product_full_name_index = mes_df.columns.get_loc('product_full_name')
        project_name_index = mes_df.columns.get_loc('project_name')
        u_product_index = uniq_products.columns.get_loc('index') 
        u_project_index = uniq_projects.columns.get_loc('index') 
        u_project_name = uniq_projects.columns.get_loc('project_name') 
        u_product_full_name = uniq_products.columns.get_loc('product_full_name') 
        mes_df["product_id"] = None
        mes_df["project_id"] = None

        for i in range(len(uniq_projects)):
            project_name = uniq_projects.iloc[i,u_project_name]
            project_id = uniq_projects.iloc[i,u_project_index]
            mes_df.loc[mes_df["project_name"]==project_name ,"project_id"] = project_id

        for i in range(len(uniq_products)):
            product_full_name = uniq_products.iloc[i,u_product_full_name]
            product_id = uniq_products.iloc[i,u_product_index]
            mes_df.loc[mes_df["product_full_name"]==product_full_name ,"product_id"] = product_id
        
        
            
        # for i in range(len(mes_df)):
        #     product_full_name = mes_df.iloc[i,product_full_name_index]
        #     project_name = mes_df.iloc[i,project_name_index]
        
        #     product_id = str(uniq_products[uniq_products["product_full_name"]==product_full_name]["index"].values[0])
            
        #     project_id = str(uniq_projects[uniq_projects["project_name"]==project_name]["index"].values[0])
            
        #     mes_df.iloc[i,m_produt_id_index] = product_id
        #     mes_df.iloc[i,m_project_id_index] = project_id
        wip.create_jobs(mes_df, ver_config["job"], ver_config["jlb"])
        
        # update wip
        wip.update_wip(mes_df, erp_df)
    # 新增Wip
    if mode == "quick_start":
        print("==== quick start ====")
        path = os.path.join('app','data','excel_template.xlsx')
        create_inventory(ver_config=ver_config, path=path, mode=mode)
        wip.create_wip(
            translate(excel_to_df(path,sheet_name="project_data")),
            ver_config)

    if mode == "custom":
        print("==== custom ====")
        path = os.path.join('app','data','input.xlsx')
        if customer_name is None:
            customer_name = "input"
        create_inventory(ver_config=ver_config, path=path, mode=mode,customer_name=customer_name)
        df = excel_to_df(path,sheet_name="project_data")
        customer_sys_mapper, reversed_map = map_keys(list(df.columns.values),customer_name)
        df = translate(df, customer_sys_mapper)
        wip.create_wip(df,ver_config)
