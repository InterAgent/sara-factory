import os
import pandas as pd
import pytz
import json
import codecs

from datetime import datetime, timedelta
from app.api.utils import convert_dfObject_to_datetime, dt_format, get_current_time, convert_datetime_to_str

def get_filetype(filepath):
    file_type = os.path.splitext(filepath)[1]
    return file_type

from openpyxl import load_workbook

def get_sheetnames_xlsx(filepath):
    wb = load_workbook(filepath, read_only=True, keep_links=False)
    return wb.sheetnames


def cut_time(df,n=32):
    bound = convert_datetime_to_str( get_current_time()+ timedelta(hours=n))
    
    if 'plan_start_time' in df.columns:
        started = df[df['real_start_time'].notna()]
        df = df[df['plan_start_time'].notna()].copy()
        if len(df) > 0:
            df = df[df['plan_start_time'] <= bound ]
        return pd.concat([started, df])
    else:
        return df

def excel_to_df(path, sheet_name=None):
    if sheet_name:
        return _clean(pd.read_excel(path, sheet_name=sheet_name, engine='openpyxl',dtype='object'))
    else:
        return pd.read_excel(path,engine='openpyxl')

def json_to_df(path):
    with codecs.open(path,'r',encoding='utf-8') as file:
        doc = json.load(file)
    return _clean(pd.DataFrame.from_dict(doc))
    
def _clean(df):
    df = df.dropna(how='all')
    df = df.loc[:, ~df.columns.str.contains('^Unnamed')]
    df = reformat_time(reformat_str(df))
    return df

def reformat_str(df):
    if "index" in df:
        df['index'] = df['index'].astype('str')
    if "project_name" in df:
        df['project_name'] = df['project_name'].astype('str')
    if "product_name" in df:
        df['product_name'] = df['product_name'].astype('str')
    if "job_name" in df:
        df['job_name'] = df['job_name'].astype('str')
    if "resource_name" in df:
        df['resource_name'] = df['resource_name'].astype('str')
    if "sourcing" in df:
        df.loc[df["sourcing"]=="內製" ,"sourcing"] = "in-house"
        df.loc[df["sourcing"]=="外包" ,"sourcing"] = "out-sourcing"
    if "project_type" in df:
        df.loc[df["project_type"]=="N" ,"project_type"] = "一般"
        df.loc[df["project_type"]=="E" ,"project_type"] = "急件"
     
    if "route_seq" in df:
        df['route_seq'] = df['route_seq'].astype('str')
    if "lot_nbr" in df:
        df['lot_nbr'] = df['lot_nbr'].astype('str')
    if "mo_nbr_step" in df:
        df['mo_nbr_step'] = df['mo_nbr_step'].astype('str')

    return df

def reformat_time(df):
    time_objects = dict(order_date="day",due="day",\
                    plan_start_time="min",plan_end_time="min",\
                    real_start_time="min",real_end_time="min",\
                    working_days="day"
                    )

    for o in time_objects:
        if o not in df.columns:
            continue
        if df[o].dtypes == object:
            df[o] = convert_dfObject_to_datetime(df[o],time_objects[o])
        else:
            df[o] = df[o].dt.strftime(dt_format(time_objects[o]))
    return df

def show_duplicate():
    pass

def check_not_empty(org_df, new_df):
    if len(org_df) != 0 and len(new_df) == 0:
        print("==== ERROR ==== ")
        print("trim return empty dataframe")
        print(org_df)
    else:
        return new_df
def trim(df,keys,subset, try_time=0):
    check_no_empty = len(df)
    index_list = [df.columns.get_loc(key) for key in keys if key in list(df.columns.values)]
    # 檢查是否一對一關係
    res = df.iloc[:, index_list].drop_duplicates(subset=subset)    
    try:
        check = df.iloc[:, index_list].drop_duplicates()
    except:
        print(index_list)
        return check_not_empty(df,res.copy())
    if subset == None:
        return  check_not_empty(df,res.copy())

    
    if len(res) != len(check):
        print("==== ERROR: ====")
        print("unexpected row value causing reindexing error")
        print(len(res), len(check))
        path = os.path.join("app","data")
        res.to_pickle(os.path.join(path,"{}_res_table".format(subset)))
        check.to_pickle(os.path.join(path,"{}_check_table".format(subset)))
        try:
            test = pd.concat([res,check]).drop_duplicates(keep=False)
            print(test)

        except Exception as e:
            print(e)

        if "order_date" in res.columns or "due" in res.columns:
            return None
        else:
            return res
        
    else:
        return  check_not_empty(df,res.copy())
       
def get_subset(df,keys):
    index_list = [df.columns.get_loc(key) for key in keys if key in list(df.columns.values)]
    return df.iloc[:, index_list]

def get_sara_key_mapper():
    path = os.path.join('app','data','sara_key_mapper.json')
    with codecs.open(path,'r',encoding='utf-8') as file:
        doc = json.load(file)
    return doc
def get_customer_key_mapper(customer_name):
    filename = customer_name + "_key_mapper.json"
    path = os.path.join('app','data',filename)
    with codecs.open(path,'r',encoding='utf-8') as file:
        doc = json.load(file)
    return doc
def get_ver_config():
    path = os.path.join('app','datagent','ver_config.json')
    with codecs.open(path,'r',encoding='utf-8') as file:
        doc = json.load(file)
    return doc
 
def save_json_to_file(data, filename, path=None):
    if not path:
        path = os.path.join('app','datagent','output',filename)
    
    with codecs.open(path, 'w', encoding='utf-8') as file:
        file.write(json.dumps(data, indent=4,  default=str))


def map_keys(customer_column,customer_name):
    # 客戶column name
    customer_keys = customer_column
    # customer-sara mapper
    customer_mapper = get_customer_key_mapper(customer_name)
    # sara-system mapper
    sara_mapper = get_sara_key_mapper()
    # sara column name
    sara_keys = sara_mapper.keys()
    # sys column name
    new_map = {}
    untracked_column = []
    for k in customer_keys:
        if k in customer_mapper:
            new_map[k] = sara_mapper[customer_mapper[k]]
        elif k in sara_mapper:
            new_map[k] = sara_mapper[k]
        else:
            untracked_column.append(k)
            continue
          
    if len(untracked_column) > 0:
        print("untrakced_column:{}".format(untracked_column))
    reversed_map = {val:key for key,val in new_map.items()}
    return new_map, reversed_map

def translate(df,mapper=None,customer_name=None):
    if mapper is None and customer_name is None:
        column_name = list(df.columns.values)
        sara_keys = get_sara_key_mapper()
        mapper = {key:val for key, val in sara_keys.items() if key in column_name}
    elif mapper is None and customer_name is not None:
        mapper, _ = map_keys(list(df.columns.values),customer_name)
    df = reformat_time(reformat_str(df.rename(columns=mapper)))
    return df
