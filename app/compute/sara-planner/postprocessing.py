from datetime import datetime, timedelta
from global_vars import RESULT, EVALUATION_TABLE

import logging
from logging.config import dictConfig
from config import *
from error_handler import PostprocessException

dictConfig(LOG_CONFIG)
logger = logging.getLogger('myLogger')

def create_timeline(resource_db:dict) -> dict:
    return {resource_id: list() for resource_id in resource_db.keys()}

def schedule_timeline(time_base:datetime, timeline:dict, resource_db:dict, job_db:dict, job_seq:dict, performance_dict:dict):
    from toolz import pipe

    jobs_assigned = list(job_seq.keys())
    primary_done = set()
    secondary_done = set()
    job_set = {'primary_resource': primary_done, 'secondary_resource': secondary_done}

    time_format = {
        'minute': "%Y-%m-%d %H:%M",
        'minutes': "%Y-%m-%d %H:%M",
        'min': "%Y-%m-%d %H:%M",
        'mins': "%Y-%m-%d %H:%M",
        'MIN': "%Y-%m-%d %H:%M",
        'MINS': "%Y-%m-%d %H:%M",
        "M": "%Y-%m-%d %H:%M",
        
        'second': "%Y-%m-%d %H:%M:%S",
        'seoncds': "%Y-%m-%d %H:%M:%S",
        'sec': "%Y-%m-%d %H:%M:%S",
        'secs': "%Y-%m-%d %H:%M:%S",
        'SEC': "%Y-%m-%d %H:%M:%S",
        'SECS': "%Y-%m-%d %H:%M:%S",
        'S': "%Y-%m-%d %H:%M:%S"
    }.get(VARS['time_granularity'], 'invalid')

    def std_out(job_id:str, resource_id:str, resource_exec:str):
        nonlocal job_set, job_db, performance_dict, timeline
        for category in job_db[job_id][resource_exec]:
            if job_id in job_set[resource_exec]:
                break

            if resource_id in job_db[job_id][resource_exec][category]:
                job_startTime = pipe(
                    time_base + timedelta(minutes=job_db[job_id]['start_time']),
                    lambda x: x.strftime(time_format)
                )
                
                job_endTime = pipe(
                    time_base + timedelta(minutes=job_db[job_id]['end_time']),
                    lambda x: x.strftime(time_format)
                )

                loading_ratio = round(performance_dict[resource_id][job_id], 2)
                timeline[resource_id].append((job_id, job_startTime, job_endTime, loading_ratio))
                job_set[resource_exec].add(job_id)
                break
    
    for resource_id in timeline.keys():
        for node in resource_db[resource_id]['queue']:
            jobs = node.get_job()
            # update: 2021/2/19
            if not jobs:
                continue

            for job_id in jobs:
                if job_id not in jobs_assigned:
                    continue

                std_out(job_id=job_id, resource_id=resource_id, resource_exec='primary_resource')
                std_out(job_id=job_id, resource_id=resource_id, resource_exec='secondary_resource')

                # update: 2021/2/17
                # if job_id not in list(job_seq.keys()):
                #     continue

                # if job_id in job_set:
                #     if not job_db[job_id]['secondary_resource']:
                #         continue
                #     else:
                #         job_startTime = str(time_base + timedelta(minutes=job_db[job_id]['start_time']))
                #         job_endTime = str(time_base + timedelta(minutes=job_db[job_id]['end_time']))
                #         loading_ratio = round(performance_dict[resource_id][job_id], 2)
                #         schedule_table[resource_id].append((job_id, job_startTime, job_endTime, loading_ratio))
                # else:
                #     job_startTime = str(time_base + timedelta(minutes=job_db[job_id]['start_time']))
                #     job_endTime = str(time_base + timedelta(minutes=job_db[job_id]['end_time']))
                #     loading_ratio = round(performance_dict[resource_id][job_id], 2)
                #     schedule_table[resource_id].append((job_id, job_startTime, job_endTime, loading_ratio))
                #     job_set.add(job_id)


def run(time_base:datetime, resource_db:dict, job_db:dict, job_seq:dict) -> dict:
    '''Execute all postprocessing steps to transform scheduled resource tie zone to datetime format.

    Args:
        time_base:
        resource_db:
        job_db:

    Returns:
        A dictionary 
    '''
    try:
        # update: 2021/2/20
        timeline = create_timeline(resource_db=resource_db)
        
        schedule_timeline(time_base=time_base,
                         timeline=timeline,
                         resource_db=resource_db,
                         job_db=job_db,
                         job_seq=job_seq,
                         performance_dict=EVALUATION_TABLE)
        
        return timeline

    except Exception as e:
        logger.error(e)
        RESULT['error'].append(str(e))