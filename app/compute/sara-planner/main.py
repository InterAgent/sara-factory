# unnecessary
import json
from datetime import datetime
from pprint import pprint

# necessary
import scheduler
import global_vars
import sys
if __name__ == '__main__':
    # update: 2021/2/17
    with open('/app/compute/input/UNASSIGNED_DB.json') as f:
        db = json.load(f)

    with open('/app/compute/input/EVENT_DB.json') as f:
        event_db = json.load(f)

    #GS = datetime.now() # change it to caller's start time
    global_vars.initialize()

    # schedule_result = {'schedule': dict, 'warning': list, 'error': list}
    GS = sys.argv[2] + ' ' + sys.argv[3]
    print(GS)
    schedule_result = scheduler.execute(db, event_db, ver='v3', seq_method=sys.argv[1] , start_time=GS)
    pprint(schedule_result) # comment it when production
    
    with open('/app/compute/output/planner_result.json', 'w') as outfile:
        json.dump(schedule_result, outfile)

