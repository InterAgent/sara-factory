class RA():
    def __init__(self, job_seq:list, job_db:dict, process_db:dict):
        self.factory = dict()
        self._process ={process_db[p]['process_name']:p for p in process_db}
        
        for pid in process_db.keys():
            self.factory[pid] = {'job_list':list(), 'next_job':None}

        job_list = tuple([i, jid, job_db[jid]['param_val']] for i, jid in enumerate(job_seq))

        for elem in job_list:
            pid = self._process[job_db[elem[1]]['process_name']]
            self.factory[pid]['job_list'].append(elem)
        
        for pid in self.factory:
            self.factory[pid]['job_list'] = tuple(self.factory[pid]['job_list'])
            sorted_list = sorted(self.factory[pid]['job_list'], key=lambda x: (x[2], x[0]), reverse=False) # sorting from small to large
            self.factory[pid]['next_job'] = take_first(sorted_list)

