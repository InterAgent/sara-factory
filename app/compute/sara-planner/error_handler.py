class PreprocessException(Exception):
    def __init__(self, code, msg):
        super().__init__(f'{code}, {msg}')

class SequencingException(Exception):
    def __init__(self, code, message):
        super().__init__(f'{code}, {message}')

class DispatchingException(Exception):
    def __init__(self, code, message):
        super().__init__(f'{code}, {message}')

class PostprocessException(Exception):
    def __init__(self, code, message):
        super().__init__(f'{code}, {message}')

class ResourceAgentException(Exception):
    def __init__(self, code, message):
        super().__init__(f'{code}, {message}')

class SchedulerAgentException(Exception):
    def __init__(self, code, message):
        super().__init__(f'{code}, {message}')