from datetime import datetime
from collections import OrderedDict
from global_vars import RESULT

import logging
from logging.config import dictConfig
from config import *
from error_handler import SequencingException

dictConfig(LOG_CONFIG)
logger = logging.getLogger('myLogger')


#============== utilities ==============
def calc_job_slack():
    pass

def calc_order_slack():
    # get job slack of last in job list
    # if job slack is None, run calc_job_slack() to generate job slack of this job list
    pass


#============== function: sort order ==============
def sort_order(order_db:dict, method:str) -> OrderedDict:
    '''Sorting orders in order_db for sequencing and dispatching

    Args:
        order_db (dict): order_data_db.

    Returns:
        A list of sorted orders
    '''
    from collections import OrderedDict

    _priority = {v: k for k, v in enumerate(VARS['order']['priority'])}

    def _rule(order, by=method):
        if by.strip() == 'order_date':
            new_key = (_priority[order['project_type']], order['order_date'], order['due'])
        elif by.strip() == 'order_due':
            new_key = (_priority[order['project_type']], order['due'], order['order_date'])
        return new_key

    return OrderedDict(sorted([(k, v) for (k, v) in order_db.items()], key=lambda x: _rule(order=x[1])))


#============== function: sort job ==============
# update: 2021/2/17
def sort_job(sorted_orders:OrderedDict, product_db:dict, job_db:dict, by_due:bool) -> OrderedDict:
    '''sort job

    Args:
        sorted_orders:
        product_db:
        job_db:
        by_due: job sequencing by job due date or not.

    Returns:

    '''
    from collections import OrderedDict

    all_products = list()

    if by_due:
        sorted_jobs = sorted(job_db.keys(), key=lambda x: (job_db[x]['due'], job_db[x]['order_due'], job_db[x]['order_date']), reverse=False)
        job_seq = OrderedDict(zip(sorted_jobs, [None]*len(sorted_jobs)))
    else:
        job_seq = OrderedDict()

    for order in sorted_orders.values():
        all_products.extend([prod for prod in order['_product']])

    for prod in all_products:
        job_bulk_list = [bulk for bulk in product_db[prod]['_jobID_list'].values()]
        for list_num, job_bulk in enumerate(job_bulk_list):
            for jobID in job_bulk:
                dependency = []
                if list_num != 0:
                    dependency.extend(job_bulk_list[list_num-1])

                pre_prodcut = job_db[jobID]['pre_product']
                if pre_prodcut:
                    pre_product_list = list(product_db[pre_prodcut]['_jobID_list'].values())
                    if pre_product_list:
                        dependency.extend(pre_product_list[-1])

                job_seq[jobID] = dependency

    return job_seq


#============== function: executor function ==============
def run(order_db:dict, product_db:dict, process_db:dict, job_db:dict, resource_db:dict, workingdays_db:dict, workinghours_db:dict, sort_by:str, time_base:datetime) -> dict:
    '''Sorting orders and jobs

    Args:
        order_db:
        product_db:
        process_db:
        job_db:
        resource_db:
        workingdays_db:
        workinghours_db:
        sort_by: order_date, order_due, job_due

    Returns:

    '''
    from collections import defaultdict
    from datetime import datetime
    from utilities import calc_job_due

    try:
        by_due = False
        calc_job_due(order_db=order_db, product_db=product_db, job_db=job_db, start_time=time_base)
        if sort_by.strip().lower() == 'job_due':
            by_due = True
            sort_order_by = 'order_due'
        else:
            sort_order_by = sort_by.strip().lower()

        order_seq = sort_order(order_db=order_db, method=sort_order_by)
        job_seq = sort_job(sorted_orders=order_seq, product_db=product_db, job_db=job_db, by_due=by_due)

        def error_call():
            raise ValueError('Invalid key')

        _sequence = defaultdict(error_call)
        _sequence['order_seq'] = order_seq
        _sequence['job_seq'] = job_seq
        return _sequence

    except Exception as e:
        logger.error(e)
        RESULT['error'].append(str(e))

