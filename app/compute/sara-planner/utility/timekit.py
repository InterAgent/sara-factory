from datetime import datetime

import logging
from logging.config import dictConfig
from config import VARS, LOG_CONFIG
from data_structure import LinkedList

dictConfig(LOG_CONFIG)
logger = logging.getLogger('myLogger')

def time_gap(start, end, granularity:str):
    from datetime import datetime

    try:
        if not isinstance(start, datetime):
            msg = '"start" must datetime type.'
            raise ValueError(msg)
        
        if not isinstance(end, datetime):
            msg = '"end" must be datetime type.'
            raise ValueError(msg)
            
        delta = (end - start).total_seconds()
        
        func = {
            'second': lambda x: x,
            'seconds': lambda x: x,
            'sec': lambda x: x,
            'minute': lambda x: x/60.0,
            'minutes': lambda x: x/60.0,
            'min': lambda x: x/60.0,
            'hour': lambda x: x/60/60,
            'hours': lambda x: x/60/60,
            'hr': lambda x: x/60/60
        }.get(granularity, lambda: 'Invalid')

        delta = round(func(delta))
        
        if delta == 'Invalid':
            msg = 'Invalid argument for "granularity".'
            raise ValueError(msg)
        else:
            return delta
        
    except Exception as e:
        logger.error(e)


def split_time_intervals_in_a_day(time_base, work_start:tuple, work_end:tuple, break_time:list, gran:str):
    from itertools import chain
    from collections import namedtuple
    from utilities import take_first, take_second
    from datetime import timedelta
    

    case = {
        'minute': 'M',
        'minutes': 'M',
        'min': 'M',
        
        'sec': 'S',
        'second': 'S',
        'seconds': 'S'
    }.get(gran.lower(), lambda: 'Invalid')
    
    
    if case == 'M':
        
        units_left = [time_base.year, time_base.month, time_base.day, time_base.hour, time_base.minute]

        # CASE: [(16, 0), (0, 0)] hour + minute
        left_bound = units_left[0: len(units_left)-len(work_start)]

        time_base = time_base + timedelta(days=1)

        units_right = [time_base.year, time_base.month, time_base.day, time_base.hour, time_base.minute]

        right_bound = units_right[0: len(units_right)-len(work_end)]


        Validstamp = namedtuple('Validstamp', ['timestamp', 'valid'])

        day_start = datetime(*left_bound)
        day_end = datetime(*right_bound)
        validstamps = [Validstamp(day_start, 0), Validstamp(day_end, 1)] # initially, avaiable all day long


        hm_timestamp_list = list(chain(*break_time))
        hm_timestamp_list.insert(0, work_start)
        hm_timestamp_list.append(work_end)

        for i, ts in enumerate(hm_timestamp_list):
            hrs = timedelta(hours=take_first(ts))
            mins = timedelta(minutes=take_second(ts))
            timestamp = day_start + hrs + mins
            valid = 0 if i%2==0 else 1
            validstamps.insert(-1, Validstamp(timestamp, valid))
    
    elif case == 'S':
        pass
        
    return validstamps


def merge_time_intervals_in_a_day(validstamps, gran:str):
    from math import isclose
    
    tol = {
        'minute': 60,
        'minutes': 60,
        'min': 60,
        
        'sec': 1,
        'second': 1,
        'seconds': 1
    }.get(gran.lower(), lambda: 'Invalid')

    merged_validstamps = list()
    num_of_validstamps = len(validstamps)
    for i,j in enumerate(range(1, num_of_validstamps)):
        delta = (validstamps[j].timestamp - validstamps[i].timestamp).total_seconds()
        if isclose(delta, 0, abs_tol=0):
            continue
        elif isclose(delta, 0, abs_tol=tol): # gap is less than 1 minute (60 seconds), but not equal
            merged_validstamps.append(validstamps[j])
        else:
            newstamp = [validstamps[i], validstamps[j]] if (j+1) == num_of_validstamps else [validstamps[i]]
            merged_validstamps.extend(newstamp)

    return merged_validstamps


def init_pattern(time_base:datetime, working_days:list, working_hours:list, break_time:list, span_size:int, gran:str):
    '''return time pattern
    time_base: datetime
    timestamp_list: e.g.[(10, 0), (15, 30)]
    '''
    from copy import deepcopy
    from namedlist import namedlist
    from data_structure import LinkedList
    from utilities import calc_time_gap, multiple_slicing

    # DEAL WITH ONE DAY
    validstamps = split_time_intervals_in_a_day(time_base=time_base,
                                                work_start=working_hours[0], work_end=working_hours[-1],
                                                break_time=break_time,
                                                gran=gran)
    
    intervals = merge_time_intervals_in_a_day(validstamps, gran)
    
    Point = namedlist('Point', ['is_available', 'on_job', 'job_qty', 'time_length'])
    pattern = []
    for idx in range(len(intervals)-1):
        on_duty = (intervals[idx].valid, intervals[idx+1].valid)
        is_available = 1 if on_duty == (0, 1) else 0 # on_duty = [0, 1] => state=1. Otherwise, state=0
        on_job = 0
        job_qty = 0
        time_length = calc_time_gap(intervals[idx].timestamp, intervals[idx+1].timestamp, VARS['time_granularity'])
        pattern.append(Point(is_available, on_job, job_qty, time_length)) # [is_available, on_job, job_qty, time_length]

    # GENERALISE TO MULTIPLE DAYS
    off_days = [idx for idx, day in enumerate(working_days) if not day]
    days_partitions = multiple_slicing(x=range(span_size), slices=off_days.copy(), stored=[])
    off_daysum = [(True, len(seg)) if seg[0] in off_days else (False, len(seg)) for seg in days_partitions]
    
    expand = []
    if len(pattern) == 1:
        pattern = pattern.pop()
        for daysum in off_daysum:
            new_pattern = deepcopy(pattern)
            if daysum[0]:
                new_pattern.is_available = 0
            expand.extend([new_pattern]*daysum[1])
    else:
        n = len(pattern)
        off_time = list()
        for d in off_days:
            off_time.extend(list(range(d*n, d*n+n)))
        for _ in range(span_size):
            new_pattern = deepcopy(pattern)
            expand.extend(new_pattern)
        for t in off_time:
            expand[t].is_available = 0
       
    expand = map(list, expand)
    expand = LinkedList(list(expand))
    return expand


def shift(llist:LinkedList, time_base):
    '''
    llist (LinkedList class): linked list object
    time_base: time reference
    max_gran: max time granularity. e.g. (hour, min)=(10, 0) => max_gran='hour'
    '''
    from toolz import pipe
    from datetime import datetime
    from data_structure import Node
    from functools import reduce
    from operator import iconcat
    from itertools import chain
    from utilities import calc_time_gap

    day_start = datetime(*[time_base.year, time_base.month, time_base.day])
    
    # step 1: Calculate shift amount
    shift_amount = calc_time_gap(day_start, time_base, VARS['time_granularity']) # day_start: day begining of input time, time_base: input time
    
    # step 2: GET node
    node = llist.get_node_by_accumulated_to(shift_amount)
    
    # step 3: Define offset_0
    if not node.get_prev():
        offset_prev = 0
    else:
        subset = llist.subset(0, node.get_index()) # not include current node
        offset_prev = sum([elem[-1] for elem in subset.elems])
        
    # step 4: Define offset_1
    offset_in_node = shift_amount - offset_prev
    
    # step 5: Define new node
    new_node = pipe(
        # flatten nested list using functools.reduce and operator.iconcat
        list(chain(*[node.get_data()[:-1], [node.get_data()[-1]-offset_in_node]])),
        ### Solution2 to flatten: reduce(iconcat, [node.get_data()[:-1], [node.get_data()[-1] - offset_in_node]], []),
        lambda x: Node(x)
    )

    # step 6: Replace node with new node
    llist.replace_node(node.get_index(), new_node)

    # step 7: Reset head of linked list
#     print(f'Before shfting: {llist}')
#     print(f'new head index: {node.get_index()}')
    llist.reset_head(node.get_index())
    
    return


