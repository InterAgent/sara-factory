'''This module provides required setting configuraton for SARA_PLANNER'''

# from random import choice
# bucket = (1.0, 1.2, 1.4, 1.6, 1.8, 2.0)
# rules = (None, 'asc', 'des')

VARS = {
    'db_keys': {
        'project_key': 'project_db',
        'process_key': 'process_db',
        'job_key': 'job_db',
        'resource_key': 'resource_db',
        'product_key': 'product_db',
        'workingdays_key': 'workingdays_db',
        'workinghours_key': 'workinghours_db'
    },
    'order': {
        'new_cols': None,
        'new_cols_type': None,
        'date_format': '%Y-%m-%d %H:%M:%S',
        'date_cols': ['order_date', 'due'],
        'order_date_col': 'order_date',
        'due_date_col': 'due',
        'priority': ['急件', '一般']
    },
    'job':{
        'new_cols':['start_time', 'end_time', 'is_finished', 'due', 'order_date', 'order_due'],
        'new_cols_default':[None, None, False, None, None, None]
    },
    'resource': {
        'new_cols': ['queue'],
        'new_cols_default': [None]
    },
    'process':{
        'new_cols': None,
        'new_cols_default': None
    },
    'workingdays': {
        'date_format': '%Y-%m-%d %H:%M:%S',
        'date_cols': ['working_days']
    },
    'workinghours': {
        'time_format': ['%H:%M', '%H:%M'],
        'time_cols': ['working_hours', 'break_time']
    },
    'time_granularity': 'minute'
}


LOG_CONFIG = {
    'version': 1,
    'disable_existing_loggers': False,
    'formatters': {
        'simpleFormatter': {
            'format': '%(asctime)s, %(name)s, %(levelname)s: %(message)s'
        }
    },
    'handlers': {
        'file': {
            'class': 'logging.FileHandler',
            'level': 'DEBUG',
            'filename': './myLog.log', # where to save the log file
            'formatter': 'simpleFormatter'
        }
    },
    'loggers': {
        'myLogger': {
            'level': 'DEBUG',
            'handlers': ['file'],
            'format': 'simpleFormatter',
            'propagate': True
        }
    }
}
