# -*- encoding: utf-8 -*-
"""
License: MIT
Copyright (c) 2019 - present AppSeed.us
"""

from flask import Flask, url_for
from flask_login import LoginManager
from werkzeug.utils import import_string
from flask_sqlalchemy import SQLAlchemy
from importlib import import_module
from logging import basicConfig, DEBUG, getLogger, StreamHandler
from os import path
from flask_restful import Resource, Api
from celery import Celery
from app.api.utils import get_current_time_string
# from celery.schedules import crontab
# from app.base.models import User


db = SQLAlchemy()
login_manager = LoginManager()

CELERY_TASK_LIST = [
    'app.tasks'
]

def create_celery_app(app=None):
    """
    Create a new Celery object and tie together the Celery config to the app's
    config. Wrap all tasks in the context of the application.

    :param app: Flask app
    :return: Celery app
    """
    from config import config_dict

    app = app or create_app(config_dict['Debug'])
    celery = Celery(app.import_name, broker=app.config['CELERY_BROKER_URL'],
                    include=CELERY_TASK_LIST)
    celery.conf.update(app.config)
    task_base = celery.Task

    class ContextTask(task_base):
        abstract = True

        def __call__(self, *args, **kwargs):
            with app.app_context():
                return task_base.__call__(self, *args, **kwargs)

    celery.Task = ContextTask
    return celery
    

def register_extensions(app):
    db.init_app(app)
    login_manager.init_app(app)

def register_blueprints(app):
    for module_name in ('base', 'home','api'):
        bp = import_string('app.{}.routes.blueprint'.format(module_name))
        print("register:{}".format('app.{}.routes.blueprint'.format(module_name)))
        #module = import_module('app.{}.routes'.format(module_name))
        #app.register_blueprint(module.blueprint)
        app.register_blueprint(bp)

def configure_database(app):

    @app.before_first_request
    def initialize_database():
#        #db.drop_all()
        from app.base.processingModels import p_SQLJobModel, p_SQLResourceModel, p_SQLProductModel, p_SQLProjectModel 
        from app.base.processingModels import p_SQLMaterialModel, p_SQLProcessModel, p_SQLJobLib, p_SQLWorkingDays,p_SQLWorkingHours 
    
        db.create_all()
        

        # """ Create default user """
        # username='test'
        # email='test@sarafactory.com'
        # password='test'
        # role = 'admin'

        # user = User.query.filter(User.email == email).first()

        # if not user:
        #     user = User(username = username, 
        #                 email = email, 
        #                 password=password,
        #                 role = role)
        #     db.session.add(user)
        #     db.session.commit()

        #print("============== DB ready! ===============")
        create_default_user()
        create_default_schedule_setting()
        create_default_bottleneck_setting()
        create_default_error_setting()



    @app.teardown_request
    def shutdown_session(exception=None):
        db.session.remove()

def create_default_user():
    # from app.base import models
    from app.base.models import User

    """ Create default user (admin) """
    username='sara'
    email='contact@interagent.io'
    password='sara'
    role = 'admin'

    user_admin = User.query.filter(User.email == email).first()

    if not user_admin:
        user_admin = User(username = username, 
                    email = email, 
                    password=password,
                    role = role)
        db.session.add(user_admin)
        db.session.commit()

        print("============== Default Admin = admin / admin ===============")

    """ Create default user (admin) """
    username='user'
    email='user@sarafactory.com'
    password='user'
    role = 'user'

    user_user = User.query.filter(User.email == email).first()

    if not user_user:
        user_user = User(username = username, 
                    email = email, 
                    password=password,
                    role = role)
        db.session.add(user_user)
        db.session.commit()

        print("============== Default User = user / user ===============")

def create_default_schedule_setting():
    from app.base.models import SQLSchedulerSetting
    if SQLSchedulerSetting.query.count() > 0:
        return
    sql_setting = SQLSchedulerSetting(
            created_time = get_current_time_string(),
            ml_model = "BPNN",
            sequencing_method = "order_due",
            dispatching_method = True
        )

    db.session.add(sql_setting)
    db.session.commit()

def create_default_bottleneck_setting():
    from app.agents.Bottleneck import BottleneckSetting
    if BottleneckSetting.get("all") != None:
        return 
    BottleneckSetting.post(info = dict(index="default",job_name="all", ignore="never", warning=4,urgent=20))

def create_default_calendar():
    pass

def create_default_shift():
    pass

def create_default_error_setting():
    from app.api.utils import import_excel
    from app.base.models import SQLErrorSetting
    error_df = import_excel(filename="error_setting.xlsx",sheet_name="error_setting").dropna(how='all')

    index_cfg = {
        "index":0,
        "error_type":1,
        "error_group":2,
        "error_code":3
    }
    for i in range(len(error_df)):
        sql_err = SQLErrorSetting.query.filter(
            SQLErrorSetting.index == error_df.iloc[i,index_cfg["index"]]).first()
        if sql_err != None:
            continue
        sql_err = SQLErrorSetting(
            index = error_df.iloc[i,index_cfg["index"]],
            error_type = error_df.iloc[i,index_cfg["error_type"]],
            error_group = error_df.iloc[i,index_cfg["error_group"]],
            error_code = error_df.iloc[i,index_cfg["error_code"]]
        )
        db.session.add(sql_err)
    db.session.commit()

def configure_logs(app):
    # soft logging
    try:
        basicConfig(filename='error.log', level=DEBUG)
        logger = getLogger()
        logger.addHandler(StreamHandler())
    except:
        pass

def apply_themes(app):
    """
    Add support for themes.

    If DEFAULT_THEME is set then all calls to
      url_for('static', filename='')
      will modfify the url to include the theme name

    The theme parameter can be set directly in url_for as well:
      ex. url_for('static', filename='', theme='')

    If the file cannot be found in the /static/<theme>/ location then
      the url will not be modified and the file is expected to be
      in the default /static/ location
    """
    @app.context_processor
    def override_url_for():
        return dict(url_for=_generate_url_for_theme)

    def _generate_url_for_theme(endpoint, **values):
        if endpoint.endswith('static'):
            themename = values.get('theme', None) or \
                app.config.get('DEFAULT_THEME', None)
            if themename:
                theme_file = "{}/{}".format(themename, values.get('filename', ''))
                if path.isfile(path.join(app.static_folder, theme_file)):
                    values['filename'] = theme_file
        return url_for(endpoint, **values)



def create_app(config, selenium=False):
    app = Flask(__name__, static_folder='base/static')

        
    app.config.from_object(config)
    if selenium:
        app.config['LOGIN_DISABLED'] = True
    register_extensions(app)
    register_blueprints(app)
    configure_database(app)
    configure_logs(app)
   
    apply_themes(app)
    #test_engine()

    return app

# def test_engine():
#     from sqlalchemy import create_engine
#     engine = create_engine("postgresql://sa100:sa100@192.168.99.101:5432/scheduleragent",encoding="utf-8", echo=True)
    

#     print("====pass create engine====")