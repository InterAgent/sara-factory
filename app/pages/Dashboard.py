from sqlalchemy import desc
from sqlalchemy import or_, and_

from app.base.models import SQLProjectModel, SQLProductModel, SQLJobModel, SQLJobLib

from app.base.models import SQLResourceModel, SQLScheduleDB, SQLProcessModel
from app.api.utils import get_current_time_string, get_current_day_string, convert_str_to_datetime, convert_datetime_to_str

from datetime import datetime, timezone, date
from datetime import timedelta 
from app import db as sql_db
def delay_count():
    count = SQLProductModel.query.filter(SQLProductModel.plan_end_time > SQLProductModel.due).count()
    return count


def plan_delivery_rate():
    total = scheduled_project_count()
    dc= delay_count()
    return "{:.2f}".format(round((1 - dc/total)*100,2))

def bottleneck_count():

    count = sql_db.session.query(SQLJobModel.bottleneck, SQLJobLib.job_name).filter(
        and_( SQLJobModel.jlb_id == SQLJobLib.id,
              SQLJobModel.bottleneck==True)).distinct().count()

    return count
def scheduled_project_count():
    #count = SQLProjectModel.query.filter(SQLProjectModel.status=="released").count()
    count = SQLProductModel.query.filter(SQLProductModel.status=="scheduled").count()
    return count

def project_count():
    return SQLProductModel.query.count()
    
def new_project_count():
    count = SQLProjectModel.query.filter(SQLProjectModel.status=="New").count()
    return count 
    

def jobs_abnormals():
    
    from app.agents import dataAgent
    return dataAgent.jobs_abnormals()

def resource_abnormals():
    count = SQLResourceModel.query.filter(SQLResourceModel.status == "pause").count()
    return count

def schedule_version():
    latest_schedule = SQLScheduleDB.query.filter(
        SQLScheduleDB.status != 'pending'
    ).order_by(desc(SQLScheduleDB.time)).first()
    if latest_schedule == None:
        status= "無排程紀錄"
    if latest_schedule.status == "scheduled":
        status = "已排程"
    elif latest_schedule.status == "released":
        status = "已發行"
    return latest_schedule.time + status

def material_delay():
    pass

def inprogress_count_product():

    return SQLProductModel.query.filter(SQLProductModel.status == "running").count()


def inprogress_count():

    return SQLJobModel.query.filter(SQLJobModel.status == "running").count()

def goal_of_the_day_count():
    today = convert_datetime_to_str(date.today())
    tomorrow = convert_datetime_to_str(date.today() + timedelta(days=1))
    count = SQLJobModel.query.filter(and_(
        SQLJobModel.plan_end_time > today,
        SQLJobModel.plan_end_time < tomorrow
        )).count()
    return count

def complete_goal_count():
    today = convert_datetime_to_str(date.today())
    tomorrow = convert_datetime_to_str(date.today() + timedelta(days=1))
    count = SQLJobModel.query.filter(and_(
        SQLJobModel.plan_end_time > today,
        SQLJobModel.plan_end_time < tomorrow,
        SQLJobModel.status  == "finished"
        )).count()
    return count

def goal_of_the_day_count_project():
    today = convert_datetime_to_str(date.today())
    tomorrow = convert_datetime_to_str(date.today() + timedelta(days=1))
    count = SQLProjectModel.query.filter(and_(
        SQLProjectModel.plan_end_time > today,
        SQLProjectModel.plan_end_time < tomorrow
        )).count()
    return count

def complete_goal_count_project():
    today = convert_datetime_to_str(date.today())
    tomorrow = convert_datetime_to_str(date.today() + timedelta(days=1))
    count = SQLProjectModel.query.filter(and_(
        SQLProjectModel.plan_end_time > today,
        SQLProjectModel.plan_end_time < tomorrow,
        SQLProjectModel.status  == "complete"
        )).count()
    return count



def available_resource_count():
    return SQLResourceModel.query.count() - resource_abnormals()
    
def idle_resource_count():
    SQLResourceEntities = SQLResourceModel.query.with_entities(SQLResourceModel.index,SQLResourceModel.jid_list).all()
    count = 0
    for sql_r in SQLResourceEntities:
        if len(sql_r.jid_list) == 0:
            count += 1

    return count

def project_query_form():
    SQLProjectEntites = SQLProjectModel.query.with_entities(SQLProjectModel.index, SQLProjectModel.project_name).all()
    return { sql_p.index:sql_p.project_name for sql_p in SQLProjectEntites}
    

def process_query_form():
    SQLProcessEntities = SQLProcessModel.query.with_entities(SQLProcessModel.index, SQLProcessModel.process_name).all()
    return { sql_process.index:sql_process.process_name for sql_process in SQLProcessEntities}
    
def jlb_name_query_form():
    SQLJLBEntities = SQLJobLib.query.with_entities(SQLJobLib.id, SQLJobLib.jlb_name, SQLJobLib.process_name).all()
    form = {}
    for sql_jlb in SQLJLBEntities:
        if sql_jlb.jlb_name not in form:
            form[sql_jlb.jlb_name] = {
                "jlb_id": [sql_jlb.id],
                "jlb_name":sql_jlb.jlb_name,
                "process_name":sql_jlb.process_name
                }
        else:
            form[sql_jlb.jlb_name]["jlb_id"].append(sql_jlb.id)
    return form


