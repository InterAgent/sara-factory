
from app.pages import Dashboard
from app import db as sql_db
from sqlalchemy import desc
from sqlalchemy import or_, and_, func
import time
import os
from datetime import datetime, timedelta
from app.base.models import SQLProjectModel, SQLScheduleDB
from app.api.utils import convert_datetime_to_str, get_current_time
def get_work_center_dashboard():
    sol = dict(
        resource_abnormal = Dashboard.resource_abnormals(), # 資源異常
        job_abnormal = Dashboard.jobs_abnormals(), # 工作異常
        remain_goals = Dashboard.goal_of_the_day_count() - Dashboard.complete_goal_count(), # 今日剩餘工作
        inprogress_count = Dashboard.inprogress_count(), # 進行中工作
        available_resource = Dashboard.available_resource_count(), # 有效資源
        idle_resource_count = Dashboard.idle_resource_count() # 閒置資源
    )
    return sol
def get_forms():
    process_list = Dashboard.process_query_form()
    jlb_list = Dashboard.jlb_name_query_form()
    return process_list, jlb_list 

def get_schedule_version():
    res = {}
    select_version = SQLScheduleDB.query.filter(
        and_(
            SQLScheduleDB.schedule_status == "mes_schedule",
            SQLScheduleDB.selected == True)).all()
    schedule_versions = SQLScheduleDB.query.filter(
        and_(
            SQLScheduleDB.schedule_status == "mes_schedule",
            SQLScheduleDB.selected != True
        )).order_by(desc(SQLScheduleDB.time)).all()
    for sql_s in select_version + schedule_versions:
        res[sql_s.id] = {
            "time": sql_s.time,
            "status": sql_s.status
        }
    return res

def get_mes_version():
    res = {}
    schedule_versions = SQLScheduleDB.query.filter(
        SQLScheduleDB.schedule_status == "mes_update"
        ).order_by(desc(SQLScheduleDB.time)).all()
    for sql_s in schedule_versions:
        res[sql_s.id] = {
            "time": sql_s.time,
            "status": sql_s.status
        }
    return res

def get_resource_card():
    from app.base.models import SQLResourceModel, SQLJobModel
    def get_card_color(sql_r):
        """
        color code:
        1 red: urgent       2 yellow: warning
        3 green: success    4 blue: idle
        """
        from app.base.models import SQLNotificationModel
        
        rid = sql_r.index
        if len(sql_r.jid_list) == 0:
            return 4, "無排定工作"
        SQLNotificationEntities = SQLNotificationModel.query.filter(
            and_(SQLNotificationModel.status!="resume_received",
                    SQLNotificationModel.rid ==rid)).first()
        
        if SQLNotificationEntities:
            return 2, SQLNotificationEntities.event_type
        else:
            return 3, None
    
    def calculate_progress(sql_j):
        from app.api.utils import get_current_time, convert_str_to_datetime

        if sql_j.real_start_time != None and sql_j.est_time != None :
            
            total = timedelta(minutes=sql_j.est_time)
            if total == timedelta(minutes=0) or total == None:
                return None
            val = get_current_time() - convert_str_to_datetime(sql_j.real_start_time)
            return round(val/total*100,2)
        else:
            return None
    def if_abnormal(sql_j):
        # return False
        from app.agents.jobAgent import JobAgent
        ja = JobAgent(jid=sql_j.index, sql_j=sql_j)
        return ja.if_abnormal()
    bound = convert_datetime_to_str( get_current_time()+ timedelta(hours=32))
    
    table = sql_db.session.query(SQLResourceModel, SQLJobModel).filter(
        and_(
        SQLJobModel.status != "finished",
        SQLJobModel.plan_end_time < bound,
        SQLResourceModel.jid_list.any(SQLJobModel.index)
        )).order_by(
            SQLJobModel.real_start_time,
            SQLJobModel.plan_start_time).all()
    
    res = {}
    for result_tuple in table:
        sql_r, sql_j = result_tuple[0], result_tuple[1]
        if sql_r.index not in res:
            
            res[sql_r.index] = dict(
                resource_name = sql_r.resource_name,
                resource_type = sql_r.resource_type,
                resource_color = None,
                event_type= None,
                job_list = []
            )
            
            res[sql_r.index]["resource_color"], res[sql_r.index]["event_type"] = get_card_color(sql_r)
            
        job_info = dict(
            job_id = sql_j.index,
            job_name = sql_j.job_name,
            jlb_id = sql_j.jlb_id,
            jlb_name = sql_j.jlb.jlb_name,
            plan_start_time = sql_j.plan_start_time,
            plan_end_time = sql_j.plan_end_time,
            real_start_time = sql_j.real_start_time,
            detail = sql_j.detail,
            process_name = sql_j.jlb.process_name,
            full_name = '[' + sql_j.product.product_full_name + ']' + sql_j.job_name,
            progress = calculate_progress(sql_j),
            status = sql_j.status,
            abnormal = if_abnormal(sql_j)
        )
        if sql_j.status == "running":

            res[sql_r.index]["job_list"].insert(0,job_info)
        else:
            res[sql_r.index]["job_list"].append(job_info)

    SQLResouceEntities = SQLResourceModel.query.all()
    for sql_r in SQLResouceEntities:
        if sql_r.index not in res:
            res[sql_r.index] = dict(
                resource_name = sql_r.resource_name,
                resource_type = sql_r.resource_type,
                resource_color = None,
                event_type= None,
                job_list = []
            )
            res[sql_r.index]["resource_color"], res[sql_r.index]["event_type"] = get_card_color(sql_r)
            

    if len(res) != len(SQLResouceEntities):
        print("mismatch", len(res), len(SQLResouceEntities))
    
    return res

def get_work_center_info():
    #dashbaord = get_work_center_dashboard()
    schedule_version = get_schedule_version()
    mes_version = get_mes_version()
    process_list, jlb_list = get_forms()
    schedule = get_resource_card()
    return [ process_list, jlb_list, schedule, schedule_version, mes_version]
    
