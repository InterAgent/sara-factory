import LocaleManager from '../../../lib/Core/localization/LocaleManager.js';
//<umd>
import LocaleHelper from '../../../lib/Core/localization/LocaleHelper.js';
import Nl from '../../../lib/Gantt/localization/Nl.js';
import SharedNl from './shared.locale.Nl.js';

const examplesNlLocale = LocaleHelper.mergeLocales(SharedNl, {

    extends : 'Nl',

    Baselines : {
        baseline           : 'baslinje',
        Complete           : 'Gedaan',
        'Delayed start by' : 'Uitgestelde start met',
        Duration           : 'Duur',
        End                : 'Einde',
        'Overrun by'       : 'Overschreden met',
        Start              : 'Begin'
    },

    Indicators : {
        Indicators     : 'Indicatoren',
        constraintDate : 'Beperking'
    },

    StatusColumn : {
        Status : 'Toestand'
    },

    TaskTooltip : {
        'Scheduling Mode' : 'Planningsmodus',
        Calendar          : 'Kalender',
        Critical          : 'Kritisch'
    }

});

LocaleHelper.publishLocale('Nl', Nl);
LocaleHelper.publishLocale('NlExamples', examplesNlLocale);

export default examplesNlLocale;
//</umd>

LocaleManager.extendLocale('Nl', examplesNlLocale);
