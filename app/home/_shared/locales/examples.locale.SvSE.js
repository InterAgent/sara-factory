import LocaleManager from '../../../lib/Core/localization/LocaleManager.js';
//<umd>
import LocaleHelper from '../../../lib/Core/localization/LocaleHelper.js';
import SvSE from '../../../lib/Gantt/localization/SvSE.js';
import SharedSvSE from './shared.locale.SvSE.js';

const examplesSvSELocale = LocaleHelper.mergeLocales(SharedSvSE, {

    extends : 'SvSE',

    Baselines : {
        Start              : 'Börjar',
        End                : 'Slutar',
        Duration           : 'Längd',
        Complete           : 'Färdig',
        baseline           : 'baslinje',
        'Delayed start by' : 'Försenad start med',
        'Overrun by'       : 'Överskridande med'
    },

    Indicators : {
        Indicators     : 'Indikatorer',
        constraintDate : 'Villkor'
    },

    StatusColumn : {
        Status : 'Status'
    },

    TaskTooltip : {
        'Scheduling Mode' : 'Läge',
        Calendar          : 'Kalender',
        Critical          : 'Kritisk'
    }

});

LocaleHelper.publishLocale('SvSE', SvSE);
LocaleHelper.publishLocale('SvSEExamples', examplesSvSELocale);

export default examplesSvSELocale;
//</umd>

LocaleManager.extendLocale('SvSE', examplesSvSELocale);
