import LocaleManager from '../../../lib/Core/localization/LocaleManager.js';
//<umd>
import LocaleHelper from '../../../lib/Core/localization/LocaleHelper.js';
import Ru from '../../../lib/Gantt/localization/Ru.js';
import SharedRu from './shared.locale.Ru.js';

const examplesRuLocale = LocaleHelper.mergeLocales(SharedRu, {

    extends : 'Ru',

    Baselines : {
        Start              : 'Начало',
        End                : 'Конец',
        Duration           : 'Длительность',
        Complete           : 'Выполнено',
        baseline           : 'базовая линия',
        'Delayed start by' : 'Задержка старта на',
        'Overrun by'       : 'Переполнен на'
    },

    Indicators : {
        Indicators     : 'Индикаторы',
        constraintDate : 'Ограничение'
    },

    StatusColumn : {
        Status : 'Статус'
    },

    TaskTooltip : {
        'Scheduling Mode' : 'Тип планирования',
        Calendar          : 'Календарь',
        Critical          : 'Критично'
    }

});

LocaleHelper.publishLocale('Ru', Ru);
LocaleHelper.publishLocale('RuExamples', examplesRuLocale);

export default examplesRuLocale;
//</umd>

LocaleManager.extendLocale('Ru', examplesRuLocale);
