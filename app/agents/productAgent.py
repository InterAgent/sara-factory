
from app.base.models import SQLProductModel, SQLJobModel, SQLJobLib

"""
web product-agent
in responsible for real time status, job sequence
"""

from app import db as sql_db
from sqlalchemy import desc
from sqlalchemy import or_, and_, func

class ProductAgent(object):
    def __init__(self,prdid,sql_product=None):
        if sql_product is None:
            self.sql_product = SQLProductModel.query.filter(
                or_(
                SQLProductModel.index == prdid,
                SQLProductModel.product_full_name ==prdid 
            )).first()
        else:
            self.sql_product = sql_product

    def pass_on(self):
        info = {
            "project_id": self.sql_product.project_id,
            "project_name": self.sql_product.project_name
        }
        return  info
    def reset(self):
        self.sql_product.status = "remove"
        self.sql_product.plan_end_time = None
        sql_db.session.commit()
        
    def start(self):
        self.sql_product.status = "running"
        sql_db.session.commit()
        
        #_project()

    def end(self):
        self.sql_product.status = "complete"
        sql_db.session.commit()
        
        self.end_project()
    
    def end_project(self):
        from app.agents.projectAgent import ProjectAgent
        p = ProjectAgent(self.sql_product.project_id)
        p.end(self.sql_product.index)
            
        
    def update_plan_end_time(self,model_set=None):

        sql_j = self.get_end_job(model_set)
        
        self.sql_product.plan_end_time = sql_j.plan_end_time
        if sql_j.plan_end_time is None:
            self.sql_product.status = "hold"
        else:
            self.sql_product.status = "scheduled"

        sql_db.session.commit()

    def get_end_job(self,model_set=None):
        if model_set != None:
            SQLJobModel = model_set[0]
        else:
            from app.base.models import SQLJobModel
        sorted_dict = [(seq,self.sql_product._jobID_list[seq]) for seq in \
            sorted(self.sql_product._jobID_list,key=int)]

        last_sequence = sorted_dict[-1][0]

        sql_j = SQLJobModel.query.with_entities(
            SQLJobModel.product_id, SQLJobModel.index, SQLJobModel.job_sequence, SQLJobModel.plan_end_time).\
            filter(and_(
                SQLJobModel.product_id == self.sql_product.index,
                SQLJobModel.job_sequence == last_sequence)).\
                    order_by(desc(SQLJobModel.plan_end_time)).first()
        
        return sql_j
    
    def get_first_ready_job(self,model_set):
        if model_set != None:
            SQLJobModel = model_set[0]
        else:
           from app.base.models import SQLJobModel 
        sql_j = SQLJobModel.query.filter(
            and_(
                SQLJobModel.product_id == self.sql_product.index,
                SQLJobModel.status != "finished"
            )
        ).order_by(SQLJobModel.plan_start_time).first()

        return sql_j
    def update_product_current_loc(self,model_set):
        self.sql_product.current_loc = self.get_first_ready_job(model_set).jlb.process_name

    def get_first_job(self):
        sorted_dict = [(seq,self.sql_product._jobID_list[seq]) for seq in \
            sorted(self.sql_product._jobID_list,key=int)]

        first_sequence = sorted_dict[0][0]
        sql_j = SQLJobModel.query.filter(
            and_(
                SQLJobModel.product_id == self.sql_product.index,
                SQLJobModel.job_sequence == first_sequence)
            ).order_by(SQLJobModel.plan_start_time).first()

        return sql_j

    def calculate_waiting_time(self):

        res = SQLJobModel.query.filter(
                SQLJobModel.product_id==self.sql_product.index
            ).with_entities(
                func.sum(SQLJobModel.waiting_time).label("waitingSum")
            ).first()

        return res.waitingSum
        
    def get_product_table(self):
        
        
        SQLJobEntities = sql_db.session.query(SQLJobModel, SQLJobLib).join(SQLJobLib).filter(
            SQLJobModel.product_id == self.sql_product.index
        ).order_by(SQLJobModel.job_sequence).all()
        
        return SQLJobEntities
    
    def get_product_bottleneck_summary(self):
        SQLJobModelEntities = sql_db.session.query(SQLJobModel, SQLJobLib)\
            .join(SQLJobLib).filter(
                and_(
                    SQLJobModel.product_id == self.sql_product.index,
                    SQLJobModel.bottleneck == True
                )
            ).all()
        return SQLJobModelEntities
    
    
    def update_buffer_time(self,due):
        from app.agents.jobAgent import JobAgent
        for jid in self.sql_product._jobID_list.values():
            ja =JobAgent(jid)
            ja.update_buffer_time(due)

    
    def get_product_info(self):
        info = {
            "project_name": self.sql_product.project_name,
            "project_id": self.sql_product.project_id,
            "product_id":self.sql_product.index,
            "product_name": self.sql_product.product_name,
            "product_status": self.sql_product.status,
            "lot_nbr": self.sql_product.lot_nbr,
            "current_loc": self.sql_product.current_loc,
            "plan_end_time": self.sql_product.plan_end_time,
            "due": self.sql_product.due
        }
        return info

