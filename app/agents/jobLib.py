import time
from flask_restful import Resource, request
from sqlalchemy import or_, and_
import pandas as pd

from app.base.models import SQLWorkingDays, SQLWorkingHours, SQLMaterialModel, SQLJobLib, SQLResourceModel, SQLProcessModel
from app import db as sql_db
print("import jobLib")

class JobLibItem(Resource):

    @staticmethod
    def post(info=None):
        encode=False
        if info is None:
            encode = True
            info = request.json

        index = "JLB" + str(SQLJobLib.query.count())
        if "job_name" not in info:
            info["job_name"] = info["jlb_name"]
        sql_jlb = SQLJobLib(**info)
        sql_jlb.index = index

        sql_db.session.add(sql_jlb)
        sql_db.session.commit()
        
        jlb_agent = JobLibAgent(sql_jlb.full_name, sql_jlb)
        jlb_agent.find_primary_second_resource()
        
        if encode == False:

            return jlb_agent.sql_jlb 
        else:
            return 200
    
    @staticmethod
    def get(full_name):
        sql_jlb = SQLJobLib.query.filter(SQLJobLib.full_name == full_name).first()
        return sql_jlb

    @staticmethod
    def put(jlb_id):
        info = request.json

        sql_jlb = SQLJobLib.query.filter(
            _or(SQLJobLib.index == jlb_id,
                SQLJobLib.id == jlb_id)).first()
        
        for key, value in info.items():
            if hasattr(sql_jlb, key):
                setattr(sql_jlb, key, value)
        sql_db.session.commit()
    
    @staticmethod
    def delete(jlb_id):
        SQLJobLib.query.filter(
            _or(SQLJobLib.index == jlb_id,
                SQLJobLib.id == jlb_id)).delete()
        sql_db.session.commit()

    @staticmethod
    def check_if_exist(full_name,info):
        sql_jlb = JobLibItem.get(full_name)
        if sql_jlb is None:
            sql_jlb = JobLibItem.post(info)
        return sql_jlb

class JobLibAgent(object):
    def __init__(self,full_name,sql_jlb=None):
        if sql_jlb is None:
            self.sql_jlb = SQLJobLib.query.filter(
                SQLJobLib.full_name == full_name
            ).first()
        else:
            self.sql_jlb = sql_jlb

    
    def find_primary_second_resource(self, model_set=None):
        if model_set != None:
            print("==== replace model ====")
            
            SQLResourceModel = model_set[0]
            SQLWorkingDays = model_set[1]
            SQLWorkingHours = model_set[2]
        else:
            from app.base.models import SQLResourceModel, SQLWorkingDays, SQLWorkingHours
        resource_count = SQLResourceModel.query.count()
        candidate_resource = {}
        job_name = self.sql_jlb.job_name if self.sql_jlb.job_name != None else self.sql_jlb.jlb_name

        if self.sql_jlb.sourcing == "in-house":
            SQLResourceEntities = SQLResourceModel.query.filter(
                and_(
                    or_(SQLResourceModel.job_name.any(job_name),
                    SQLResourceModel.resource_name == job_name + '-未註冊機台' #檢查virtual machine
                    ),
                    or_(SQLResourceModel.resource_type != "vendor",
                        SQLResourceModel.resource_type == None)
                    )
                ).all()
        elif self.sql_jlb.sourcing  == "out-sourcing":
            SQLResourceEntities = SQLResourceModel.query.filter(
                and_(
                    SQLResourceModel.job_name.any(job_name),
                    or_(SQLResourceModel.resource_type == "vendor",
                        SQLResourceModel.resource_type == None)
                    )
                ).all()
        #未註冊資源
        
        if job_name in ["外發入料",'二次防焊','貼底片']:
            print(job_name,len(SQLResourceEntities))
        
        if len(SQLResourceEntities) == 0:
            #未展開工作
            SQLResourceEntities = []
            if self.sql_jlb.process_name == job_name:
                sql_r = SQLResourceModel(
                    index = "RCID" + str(resource_count),
                    resource_name = job_name + '-未註冊機台',
                    job_name = [job_name],
                    resource_type = "process",
                    virtual_resource = True
                )
                sql_wd = SQLWorkingDays.query.filter(SQLWorkingDays.index == "WDID0").first()
                sql_wh = SQLWorkingHours.query.filter(SQLWorkingHours.hours_name == "全天").first()
                sql_r.workingDays = sql_wd
                sql_r.workingHours = sql_wh
                sql_db.session.add(sql_r)
                SQLResourceEntities.append(sql_r)
                resource_count +=1
            else:
                status_name = job_name + '-未註冊機台'
                SQLResourceEntities = SQLResourceModel.query.filter(SQLResourceModel.resource_name == status_name).all()
                if len(SQLResourceEntities) == 0:
                    if job_name in ["外發入料",'二次防焊','貼底片']:
                        print("create new resource")
                    sql_r = SQLResourceModel(
                        index = "RCID" + str(resource_count),
                        resource_name = job_name + '-未註冊機台',
                        job_name = [job_name + '-未註冊工作'],
                        resource_type = "virtual",
                        virtual_resource = True
                    )
                    sql_wd = SQLWorkingDays.query.filter(SQLWorkingDays.index == "WDID0").first()
                    sql_wh = SQLWorkingHours.query.filter(SQLWorkingHours.hours_name == "全天").first()
                    sql_r.workingDays = sql_wd
                    sql_r.workingHours = sql_wh

                    sql_db.session.add(sql_r)
                    resource_count +=1
                    SQLResourceEntities = [sql_r]
                else:
                    if job_name in ["外發入料",'二次防焊','貼底片']:
                        print("found resource")

        sql_db.session.commit()
        secondary_resource = {}
        primary_resource = {}

        
        for sql_r in SQLResourceEntities:
            
            if sql_r.resource_type == 'operator':
                if 'operator' not in primary_resource:
                    primary_resource['operator'] = []    
                primary_resource['operator'].append(sql_r.index)

            if sql_r.resource_type != 'operator':
                if sql_r.resource_type not in secondary_resource:
                    secondary_resource[sql_r.resource_type]=[]
                secondary_resource[sql_r.resource_type].append(sql_r.index)
        if len(primary_resource) == 0:
            primary_resource = secondary_resource
            secondary_resource = {}

        #print("--- find candidate resource: %s seconds ---" % (time.time() - start_time))
        self.sql_jlb.primary_resource = primary_resource
        self.sql_jlb.secondary_resource = secondary_resource
        
        sql_db.session.commit()