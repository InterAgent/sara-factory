from sqlalchemy import or_, and_
from sqlalchemy import cast, literal
from sqlalchemy.orm.attributes import flag_modified    
from datetime import datetime, timedelta
import time
import pandas as pd
import json
import os
from app.base.models import SQLProjectModel, SQLProductModel, SQLJobModel, SQLProcessModel, SQLSchedulerSetting, SQLMaterialModel, SQLJobLib
from app.base.models import SQLWorkingDays, SQLWorkingHours, SQLResourceModel, SQLScheduleDB, SQLNotificationModel, SQLErrorSetting 
from app import db as sql_db

from app.api.utils import import_json, latest_file, import_excel, convert_str_to_datetime, convert_datetime_to_str
from app.api.utils import convert_dfObject_to_datetime, dt_format, get_current_time_string, get_current_time
print("import dataAgent")
def get_inprogress_jobs():
    tupleEntities = sql_db.session.\
        query(SQLJobModel, SQLJobLib).\
        join(SQLJobLib).\
        filter(SQLJobModel.status=="running").\
        order_by(SQLJobModel.project_id, SQLJobModel.plan_start_time).all()
    
    table = AlchemyEncoder().default(tupleEntities,mode="time")
    # time_spend_info = get_time_spent(tupleEntities) 
    # for jid in time_spend_info:
    #     table[jid]["time_spend"] = time_spend_info[jid]
    return table
    

def get_latest_schedule():
    res = SQLScheduleDB.query.filter(SQLScheduleDB.status=="released").\
            order_by(desc(SQLScheduleDB.time)).first()
    return res

def pause_following_jobs(current_sql_j):

    SQLJobEntities = SQLJobModel.query.filter(
        and_(
            SQLJobModel.product_id == current_sql_j.product_id,
            SQLJobModel.job_sequence > current_sql_j.job_sequence 
        )).all()
    
    for sql_j in SQLJobEntities:
        #print(sql_j)

        sql_j.status = "pause"
        sql_db.session.commit()

def resume_following_jobs(current_sql_j):

    SQLJobEntities = SQLJobModel.query.filter(
        and_(
            SQLJobModel.product_id == current_sql_j.product_id,
            SQLJobModel.job_sequence > current_sql_j.job_sequence 
        )).all()
    
    for sql_j in SQLJobEntities:
        #print(sql_j)

        sql_j.status = "resume"
        sql_db.session.commit()


def find_lock_time(sql_r):
    return 0

def find_bottleneck_threshold(job_name):
    from app.agents.Bottleneck import BottleneckSetting
    sql_b = BottleneckSetting.get(job_name)
    return sql_b.warning, sql_b.urgent, sql_b.ignore

def find_working_hours_mismatach_threshold(sql_j):
    return 20

def find_operation_delay_threshold(sql_j):
    return 20

def jobs_abnormal_table():
    from app.agents.jobAgent import JobAgent
    start_time = time.time()
    SQLJobEntities = SQLJobModel.query.filter(SQLJobModel.status!= "remove").all()
    jobs_operation_delay = 0
    jobs_working_hours_mismatch = 0
    jobs_abnormal_sequence = 0
    jobs_abnormal_report = 0

    sol = {
        "operation_delay":{},
        "working_hours_mismatch":{},
        "abnormal_sequence":{},
        "abnormal_report":{}
    }    

    for sql_j in SQLJobEntities:
        jid = sql_j.index
        ja = JobAgent(sql_j.index, sql_j)
        this = ja.get_job_info()
        this.update(ja.get_time_info())
        this["abnormal_status"] = ja.sql_j.abnormal_status

        if ja.abnormal_operation_delay() > 0:
            sol["operation_delay"][jid] = this

        abnormal_status = sql_j.abnormal_status
        if abnormal_status["working_hours_mismatch"] == True:
            sol["working_hours_mismatch"][jid] = this

        if abnormal_status["abnormal_sequence"] == True:
            sol["abnormal_sequence"][jid] = this
             
        if abnormal_status["abnormal_report"] > 0:
            sol["abnormal_report"][jid] = this
            
            events = ja.get_abnormal_history()
            sol["abnormal_report"][jid]["event_start_time"] = events[0]["event_time"]
            sol["abnormal_report"][jid]["event_type"] = events[0]["event_type"]
    return sol
             
def jobs_abnormals():
    
    from app.agents.jobAgent import JobAgent
    start_time = time.time()
    bound = convert_datetime_to_str( get_current_time()+ timedelta(hours=32))
    SQLJobEntities = SQLJobModel.query.with_entities(
        SQLJobModel.index, SQLJobModel.abnormal_status
        ).filter(
            and_(
                SQLJobModel.status!= "remove",
                SQLJobModel.plan_end_time <= bound
            )).all()
    jobs_operation_delay = 0
    jobs_working_hours_mismatch = 0
    jobs_abnormal_sequence = 0
    jobs_abnormal_report = 0

    for sql_j in SQLJobEntities:
        jid = sql_j.index
        
        ja = JobAgent(jid)
        if ja.abnormal_operation_delay() > 0:
            jobs_operation_delay +=1
        abnormal_status = sql_j.abnormal_status

        if abnormal_status["working_hours_mismatch"] == True:
            jobs_working_hours_mismatch +=1
        if abnormal_status["abnormal_sequence"] == True:
            jobs_abnormal_sequence +=1
        if abnormal_status["abnormal_report"] > 0:
            jobs_abnormal_report += abnormal_status["abnormal_report"] 
    print("calculate jobs abnormal--- %s seconds ---" % (time.time() - start_time))
    return (jobs_operation_delay + jobs_working_hours_mismatch + \
           jobs_abnormal_sequence + jobs_abnormal_report) 



def distribute_jobs(job_db,product_db):
    from app.agents.jobAgent import JobAgent
    start_time = time.time()
    SQLJobEntities = SQLJobModel.query.filter(
        SQLJobModel.status != "remove"
    ).order_by(
        SQLJobModel.plan_start_time,
        SQLJobModel.index).all()
    
    visited = set()
    skipped = set()
    flag = False    
    full_jid = {sql_j.index:sql_j for sql_j in SQLJobEntities}
    
    for prd in product_db:
        for seq, jid_list in product_db[prd]["_jobID_list"].items():
            for jid in jid_list:
                print(jid)
                if jid not in full_jid:
                    
                    create_job(jid, job_db[jid])
                else:
                      
                    ja = JobAgent(jid,full_jid[jid])
                    visited.add(jid)
                    
                    if job_db[jid]["product_id"] != ja.sql_j.product_id:
                        ja.sql_j.product_id = job_db[jid]["product_id"]
                    
                    ja.update_assigned_resource(
                        resource=job_db[jid]["resource"]
                    )
                    ja.update_plan_working_time(
                        plan_start_time=job_db[jid]["plan_start_time"],
                        plan_end_time=job_db[jid]["plan_end_time"]
                    )
    sql_db.session.commit()
    print("--- %s seconds ---" % (time.time() - start_time))


def DA_calculte_waiting_time():
    from app.agents.jobAgent import JobAgent
    print("calculate DA_calculte_waiting_time")
    start_time = time.time()
    s = sql_db.session
    SQLJobEntities = s.query(SQLJobModel).filter(
        SQLJobModel.status != "remove"
    ).order_by(
        SQLJobModel.plan_start_time,
        SQLJobModel.index).all()

    for sql_j in SQLJobEntities:
        ja = JobAgent(sql_j.index, sql_j)
        ja.calculate_waiting_time(mode="group")
    s.commit()
    print("--- %s seconds ---" % (time.time() - start_time))


def calculate_bottlenecks():
    from app.agents.jobAgent import JobAgent
    print("calculate bottleneck")
    start_time = time.time()
    s = sql_db.session
    SQLJobEntities = s.query(SQLJobModel).filter(
        SQLJobModel.status != "remove"
    ).order_by(
        SQLJobModel.plan_start_time,
        SQLJobModel.index).all()


    for sql_j in SQLJobEntities:
        
        ja = JobAgent(sql_j.index, sql_j)
        ja.check_bottleneck(mode="group")
    s.commit()
    print("--- %s seconds ---" % (time.time() - start_time))

def calculate_real_working_hours():
    pass
    
def calculate_working_hours():
    from app.agents.jobAgent import JobAgent
    print("calculate working_hours")
    start_time = time.time()
    s = sql_db.session
    table = s.query(SQLResourceModel, SQLJobModel).filter(
        and_(
        SQLJobModel.status != "remove",
        SQLJobModel.plan_start_time != None,
        SQLJobModel.plan_end_time != None,
        SQLResourceModel.jid_list.any(SQLJobModel.index)
        )).order_by(
            SQLJobModel.plan_start_time,
            SQLJobModel.index).all() 
    
    # SQLJobEntities = s.query(SQLJobModel).filter(
    #     and_(
    #         SQLJobModel.status != "remove",
    #         SQLJobModel.plan_start_time != None,
    #         SQLJobModel.plan_end_time != None
    #     )        
    # ).order_by(
    #     SQLJobModel.plan_start_time,
    #     SQLJobModel.index).all() 
    count = 0 
    for res_tuple in table:
        sql_j = res_tuple[1]
        sql_r = res_tuple[0]
        
        ja = JobAgent(sql_j.index, sql_j)
        try:
            ja.calculate_plan_working_hours_v2(sql_j, sql_j.plan_start_time, sql_j.plan_end_time, sql_r)
        except Exception as e:
            print("calculate_working_hours failed, {} {}".format(sql_j.index,e))
        count +=1
        if count % 10 == 0:
            s.commit()
            print("===== {}/{} =====".format(count,len(table)))
        print(sql_j.index)
    s.commit()
    print("--- %s seconds ---" % (time.time() - start_time))

def create_job(jid,info):
    from app.agents.jobAgent import JobAgent

    mother = JobAgent(info["mother"]["jid"])
    mother_info = mother.pass_on()
    info.update(mother_info)    
    sql_j = SQLJobModel(**info)
    sql_j.index = jid
    
    ja = JobAgent(sql_j.index,sql_j) 
    
    try:
        ja.update_plan_working_time(
        plan_start_time=info["plan_start_time"],
        plan_end_time=info["plan_end_time"]
        )
        mother.reset()
    
    except Exception as e:
        print(e)
        print(jid)
        print(info)
    sql_db.session.add(sql_j)
    sql_db.session.commit()


def distribute_resources(schedule):

    from app.agents.resourceAgent import ResourceAgent
    start_time = time.time() 
    for rid in schedule:
        ra = ResourceAgent(rid)
        ra.update_assigned_jobs(schedule[rid])
    print("--- %s seconds ---" % (time.time() - start_time))
        
def update_product(product_db):
    from app.agents.productAgent import ProductAgent
    start_time = time.time() 
    for prdid in product_db:
        product_agent = ProductAgent(prdid)
        product_agent.update_plan_end_time()
    print("--- %s seconds ---" % (time.time() - start_time))

def create_product(product_db):
    from app.agents.productAgent import ProductAgent
    start_time = time.time()
    SQLProductEntities = SQLProductModel.query.with_entities(SQLProductModel.index).all()
    current_product_set = set([sql_product.index for sql_product in SQLProductEntities])
    update_product_set = set(product_db.keys())
    new_product = update_product_set - current_product_set
    print(new_product)
    for prdid in new_product:
        product_agent = ProductAgent(prdid)
        mother = ProductAgent(product_db[prdid]["mother"])
        mother_info = mother.pass_on()
        info = product_db[prdid]
        info.update(mother_info)
        product_agent.sql_product = SQLProductModel(**info)
        product_agent.sql_product.index = prdid 
        sql_db.session.add(product_agent.sql_product)
        sql_db.session.commit()
        mother.reset()
    print("--- %s seconds ---" % (time.time() - start_time))

def update_project(project_db):
    from app.agents.projectAgent import ProjectAgent
    start_time = time.time() 
    for pid in project_db:
        
        pa = ProjectAgent(pid)
        if pa.sql_p._product != project_db[pid]["_product"]:
            pa.sql_p._product = project_db[pid]["_product"]
            sql_db.session.commit()
             
        pa.update_plan_end_time()
    print("--- %s seconds ---" % (time.time() - start_time))

def create_process(info):
    count = SQLProcessModel.query.count()
    index = "PRCID" + str(count)
    sql_process = SQLProcessModel(**info)
    sql_process.index = index
    sql_db.session.add(sql_process)
    sql_db.session.commit()

def remove_complete_jobs():    
    res = sql_db.session.query(SQLProductModel, SQLJobModel).filter(
        and_(
            SQLJobModel.product_id == SQLProductModel.index,
            SQLJobModel.status == "finished")
        ).all()

    for result_tuple in res:
        sql_product = result_tuple[0]
        sql_j = result_tuple[1]
        del sql_product._jobID_list[str(sql_j.job_sequence)]
        
        flag_modified(sql_product, "_jobID_list")

    sql_db.session.commit()
def group_check_job_abnormal():
    from app.agents.jobAgent import JobAgent
    
    finished_jobs = SQLJobModel.query.filter(
        and_(
            SQLJobModel.status=="finished",
            SQLJobModel.plan_start_time != None,
            SQLJobModel.plan_end_time != None,
            SQLJobModel.real_start_time != None,
            SQLJobModel.real_end_time != None)).all()
    time_mismatched = 0
    for sql_j in finished_jobs:
        ja = JobAgent(sql_j.index,sql_j)
        if ja.abnormal_working_hours_mismatch():
            time_mismatched +=1
    sql_db.session.commit()

    running_jobs = SQLJobModel.query.filter(SQLJobModel.status=="running").all()
    abseq_count = 0
    for sql_j in running_jobs+finished_jobs:
        ja = JobAgent(sql_j.index,sql_j)
        if ja.abnormal_sequence():
            abseq_count +=1

    sql_db.session.commit()

    print("==== abnormal_seq count:{}, time_mismatched:{}".format(abseq_count,time_mismatched))
    
def expand_job_detail():
    # find job whose job_detail is not full
    def find_jobs_with_detail():
        result = sql_db.session.query(SQLJobModel).filter(
            cast(SQLJobModel.detail, sql_db.String) != cast(literal(sql_db.JSON.NULL, sql_db.JSON()), sql_db.String)
            ).all()
        return result
    def find_jobs_with_the_same_plan(plan_start_time,plan_end_time):
        SQLJobEntities = SQLJobModel.query.filter(
        and_(SQLJobModel.plan_start_time == plan_start_time,
             SQLJobModel.plan_end_time == plan_end_time
            )).all()
        return SQLJobEntities
    def translate_plan_detail(detail, match_jobs,current_sql_j):
        info = {
            "opens": detail["opens"],
            "同時作業": [(j.index,j.product_name+j.job_name) for j in match_jobs 
                        if (
                            j.index != current_sql_j.index and 
                            j.jlb.jlb_name == current_sql_j.jlb.jlb_name
                        ) ]
        }
        return info
    
    jobs_with_detail = find_jobs_with_detail()
    visited = []
    for job in jobs_with_detail:
        if type(job.detail) != list or job.index in visited:
            continue
        for plans in job.detail:
            for _id in plans:
                partner_jobs = find_jobs_with_the_same_plan(plans[_id]["start_time"][:-3], plans[_id]["end_time"][:-3])
                if len(partner_jobs) == 0:
                    continue
                else:
                    visited.extend([sql_j.index for sql_j in partner_jobs])
                    for sql_j in partner_jobs:
                        sql_j.translated_detail.append(translate_plan_detail(plans[_id], partner_jobs, sql_j))
    sql_db.session.commit()
    

