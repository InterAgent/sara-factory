# author: Joy
# Updated On 2021-01-06

import json
import codecs
import os
import pytz
import pandas as pd

DATA_PATH = os.path.join('app','data')

from datetime import datetime, timezone

def get_timezone():
    tz = pytz.timezone('Asia/Taipei')
    return tz

def dt_format(mode):
    if mode == "day":
        dt_format = '%Y-%m-%d'
    elif mode =="hour":
        dt_format = '%H:%M'
    elif mode == "min":
        dt_format = '%Y-%m-%d %H:%M'
    elif mode == "12":
        dt_format = "%I:%M%p"
    elif mode == "12to24":
        dt_format = "%H:%M"
    else:
        dt_format = '%Y-%m-%d %H:%M'

    return dt_format

def convert_dfObject_to_datetime(col,mode="min"):
    return pd.to_datetime(col, errors='coerce').dt.strftime(dt_format(mode))
                 
def convert_str_to_datetime(string,mode="min"):
    try:
        string = datetime.strptime(string, dt_format(mode))
    except:
        
        try: 
            string = datetime.strptime(string, dt_format("min")) 
        except Exception as e:
            print(e)
            print(string, dt_format(mode))
        
    return string.replace(tzinfo=get_timezone())

def convert_datetime_to_str(dt, mode="min"):
    
    try:
        result = dt.strftime(dt_format(mode))
    except:
        dt_f = '%Y-%m-%d %H:%M:%S.%f'
        result = dt.strftime(dt_f)

    return result

def convert_datetime_to_iso_tz(dt):
    pass
    
def get_current_time_string():
    tz = get_timezone()
    time_now = datetime.now(tz).strftime(dt_format(mode="min"))
    # # taipei_now = datetime.now(tz).isoformat()
     
    return time_now

def get_current_day_string():
    tz = get_timezone()
    return datetime.now(tz).strftime(dt_format(mode="day"))

def get_current_time():
    tz = get_timezone()
    return datetime.now(tz)    
    
def get_current_time_iso_tz():
    return datetime.now().replace(tzinfo=timezone.utc).isoformat()

def import_json(filename, path=None):
    if not path:
        path = os.path.join(DATA_PATH,filename)
        
    with codecs.open(path,'r',encoding='utf-8') as file:
        doc = json.load(file)
    return doc

def save_json_to_file(data, filename, path=None):
    if not path:
        path = os.path.join(DATA_PATH,filename)
    
    with codecs.open(path, 'w', encoding='utf-8') as file:
        file.write(json.dumps(data, indent=4,  default=str))

def latest_MES_file():
    SCHEDULE_PATH = os.path.join('app','schedule_result',"*.json")
    import glob
    path = os.path.join(SCHEDULE_PATH)
    list_of_files = glob.glob(path) # * means all if need specific format then *.csv
    if len(list_of_files) ==0:
        return None
    latest_file_path = max(list_of_files, key=os.path.getctime)
    return latest_file_path
    
def latest_file():
    import glob
    path = os.path.join(DATA_PATH,"schedule_*")
    list_of_files = glob.glob(path) # * means all if need specific format then *.csv
    latest_file_path = max(list_of_files, key=os.path.getctime)
    return latest_file_path

def latest_gantt_file():
    import glob
    path = os.path.join(DATA_PATH,"gantt_*")
    list_of_files = glob.glob(path) # * means all if need specific format then *.csv
    latest_file_path = max(list_of_files, key=os.path.getctime)
    return latest_file_path

def import_csv(filename):
    import pandas as pd
    return pd.read_csv(os.path.join(DATA_PATH,filename))  

def serialize_db(db):
    json_db = {}
    for item in db:
        vals =  list(vars(item))[1:]
        if "index" in vals:
            index = item.index
        else:
            index = item.id 
        json_db[index]={}
        for val in vals:
            if val == "id": 
                continue
            if isinstance(getattr(item, val),set):
                json_db[index][val]=list(getattr(item, val))
            else:
                json_db[index][val] = getattr(item, val)
    print(json_db)
    return json_db

def serialize_first_query(row):
    sol = {}
    vals = list(vars(row))[1:]
    for val in vals:
        if isinstance(getattr(row, val),set):
            sol[val]=list(getattr(row, val))
        else:
            sol[val] = getattr(row, val)
    return sol
 
def serialize_db_with_entities(result):
    sol = {}
    for e, item in enumerate(result):
        row = item._asdict()
        keys = list(row.keys())
        if "index" in keys:
            index = item.index
        else:
            index = e
        
        sol[index] = {}
            
        for key in keys:
            if isinstance(getattr(item, key),set):
                sol[index][key]=list(getattr(item, key))
            else:
                sol[index][key] = getattr(item, key)
    return sol
                
        

def import_excel(filename, path=None, sheet_name=None):
    import pandas as pd
    df1 = pd.read_excel(os.path.join(DATA_PATH,filename), sheet_name=sheet_name, engine='openpyxl') 
    return df1

from sqlalchemy.ext.declarative import DeclarativeMeta
class AlchemyEncoder(json.JSONEncoder):
    def default(self, o, mode="json"):
        if isinstance(o, list):
            data = {}
            for tup in o:
                
                index = tup[0].index if hasattr(o[0], 'index') else tup[0]._id
                if index not in data:
                    data[index] = {}
                
                for obj in tup:
                    data[index].update(self.parse_sqlalchemy_object(obj,mode)) 
                    #print(data[index])
            return data
             
        if isinstance(o, tuple):
            
            data = {}
            for obj in o:
                data.update(self.parse_sqlalchemy_object(obj))
            return data
        if isinstance(o.__class__, DeclarativeMeta):
            return self.parse_sqlalchemy_object(o)
        return json.JSONEncoder.default(self, o)

    def parse_sqlalchemy_object(self, o, mode="json"):
        data = {}
        if mode == "json":
            fields = o.__json__() if hasattr(o, '__json__') else dir(o)
        else:
            fields = o.__time__() if hasattr(o, '__time__') else dir(o)
        for field in [f for f in fields if not f.startswith('_') and f not in ['metadata', 'query', 'query_class','index']]:
            
            value = o.__getattribute__(field)
            try:
                json.dumps(value)
                data[field] = value
            except TypeError:
                data[field] = None
        return data
    