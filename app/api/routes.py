from flask import Blueprint
from flask_restful import Resource, Api

blueprint = Blueprint(
    'api_blueprint',
    __name__,
    url_prefix='/api',
    template_folder='templates',
    static_folder='static'
)

# from app.api.services import Reschedule, DBReschedule
# from app.api.services import GetSchedule


#from app.api.services import Split, Extend, Add
from app.api.services import Split
from app.api.quickStart import QuickStart
from app.api.schedulerAPI import Planner, Schedule
from app.api.gantt_services import GanttFormat, LoadGantt, SyncGantt

from app.api.functionsAPI import CreateSchedulerSetting, UpdateDB
from app.api.functionsAPI import GetScheduleJSON, MESUpdate

from app.api.modelAPI import UpdateJob, Notification

from app.api.pagesAPI import EditProduct
from app.api.pagesAPI import GetProject, GetResource, ScheduleDetail, GetProduct, ManageJobs, GetMaterial, ManageResources
from app.api.pagesAPI import GetSchduleProgress
from app.api.pagesAPI import WorkCenterInfo, ErrorSetting, WorkingHour, WorkingDay, GetOutSourcing, GetInprogressJobs
from app.api.pagesAPI import Dashboard, ScheduleComparision, ResourcePerformance, JobPerformance, DelayOrder, BottleneckAnalytics

from app.agents.Bottleneck import BottleneckSetting
from app.agents.jobLib import JobLibItem
from app.agents.jobAgent import GetJob
api = Api(blueprint)

# api.add_resource(Reschedule, "/update_status")
# api.add_resource(DBReschedule, "/update_sql_status")
# api.add_resource(GetProcess, "/get_process")
# api.add_resource(GetSchedule, "/get_schedule/<index>","/get_schedule",endpoint='GetSchedule')

api.add_resource(GetScheduleJSON, "/get_schedule_json")

api.add_resource(Split, "/split/<req>")
# api.add_resource(Extend, "/dpe/<jid>")
# api.add_resource(Add, "/add/<jid>")

api.add_resource(GanttFormat, "/gantt_format")
api.add_resource(LoadGantt, "/load_gantt")
api.add_resource(SyncGantt, "/sync_gantt")

api.add_resource(GetProject, "/get_project/<pid>","/get_project",endpoint='GetProject')
api.add_resource(GetResource, "/get_resource/<rid>","/get_resource",endpoint='GetResource')
api.add_resource(GetJob,"/get_job/<jid>","/get_job",endpoint='GetJob')
api.add_resource(GetProduct,"/get_product/<prdid>","/get_product",endpoint="GetProduct")
api.add_resource(EditProduct, "/edit_product")
api.add_resource(JobLibItem, "/get_job_lib_item/<jlb_id>","/get_job_lib_item",endpoint="JobLibItem")
api.add_resource(Schedule,"/get_schedule/<sid>","/get_schedule",endpoint="Schedule")
api.add_resource(ManageJobs,"/get_job_list")
api.add_resource(ManageResources,'/manage_resources','/manage_resources/<method>',endpoint='ManageResources')
api.add_resource(GetMaterial,"/get_material")
api.add_resource(GetInprogressJobs,"/get_inprogress_jobs")

api.add_resource(QuickStart, "/quick_start/<mode>")
api.add_resource(Planner, "/update_status_planner")
api.add_resource(UpdateDB, "/update_db")
api.add_resource(MESUpdate,"/mes_update","/mes_update/<sid>",endpoint="MESUpdate")

api.add_resource(CreateSchedulerSetting,"/scheduler_setting")

api.add_resource(Notification,'/get_notification','/get_notification/<nid>',endpoint="nid")
api.add_resource(GetSchduleProgress, '/get_schedule_progress')

api.add_resource(ScheduleDetail,"/get_schedule_detail")
api.add_resource(WorkCenterInfo, '/get_work_center_info')

api.add_resource(Dashboard,'/get_dashboard_info','/get_dashboard_info/<method>',endpoint="Dashboard")
api.add_resource(ScheduleComparision,'/get_schedule_comparision','/get_schedule_comparision/<index_pair>',endpoint="GetScheduleComparision")
api.add_resource(ResourcePerformance, "/get_resource_performance")
api.add_resource(DelayOrder, "/get_delay_order/<product_id>","/get_delay_order",endpoint="delay")

api.add_resource(JobPerformance,"/get_job_performance")


api.add_resource(BottleneckSetting,'/get_bottleneck_setting', '/get_bottleneck_setting/<jlb_name>',endpoint="bottleneckSetting")
api.add_resource(BottleneckAnalytics,'/get_bottleneck','/get_bottleneck/<jlb_name>',endpoint="bottleneck")

api.add_resource(GetOutSourcing, '/get_outsourcing')


api.add_resource(WorkingHour, '/get_shift','/get_shift/<whid>',endpoint="shift")
api.add_resource(WorkingDay, '/get_calendar/<calendar_name>','/get_calendar',endpoint="calendar")
api.add_resource(ErrorSetting,'/get_error_setting','/get_error_setting/<eid>',endpoint="error")
api.add_resource(UpdateJob,'/update_job/<args>')