# author: Joy
# Updated On 2020-12-22
# Detail: Update jobID_list Format
from sqlalchemy import desc
from sqlalchemy import or_, and_

from app.base.models import SQLProjectModel, SQLProductModel, SQLJobModel, SQLProcessModel, SQLSchedulerSetting, SQLMaterialModel
from app.base.models import SQLWorkingDays, SQLWorkingHours, SQLResourceModel, SQLScheduleDB, SQLNotificationModel

from app.api.utils import get_current_time_string, get_current_day_string, convert_str_to_datetime, convert_datetime_to_str

from datetime import datetime, timezone
from datetime import timedelta 

def update_db(db,schedule,name):
    resource_db = db["resource_db"]
    job_db = db["job_db"]
    project_db = db["project_db"]
    product_db = db["product_db"]

    db["schedule"] = {
        "time":name,
        "res":schedule
    }
    
    for RID in schedule:
        resource_name = resource_db[RID]["resource_name"] 
        for job_tuple in schedule[RID]:
            
            JID = job_tuple[0]
            
            PRDID = job_db[JID]["product_id"]
            PRJID = product_db[PRDID]["project_id"]
            start_time = job_tuple[1].split('.')[0]
            end_time = job_tuple[2].split('.')[0]
            job_db[JID]["plan_start_time"] = start_time
            job_db[JID]["plan_end_time"] = end_time
            if "resource" not in job_db[JID]:
                
                job_db[JID]["resource"] = {}
            job_db[JID]["resource"][RID]= resource_name
            
            project_db[PRJID]["updated_on"] = get_current_time_string()
    return db

class SplitJob(object):
    def __init__(self,db):
        self.db = db
    
    @staticmethod
    def copy(old_job):
        new_job = {}
        for key, values in old_job.items():
            new_job[key]=values
        return new_job

    @staticmethod 
    def split(job, num, sequence, ratio=None, divide=True):
        if divide and ratio == None:
            ratio = num/job["qty"]
            new_job = SplitJob.copy(job)
            new_job["qty"] = num
        elif divide and ratio:
            new_job = SplitJob.copy(job)
            new_job["qty"] = job["qty"]*ratio
        else:
            new_job = SplitJob.copy(job)
            new_job["qty"] = job["qty"]-(job["qty"]*ratio)
        
        new_job["job_sequence"]=sequence
        
        if divide:
            new_job["product_id"] = job["product_id"]+"_001"
            return ratio, new_job
        else:
            new_job["product_id"] = job["product_id"]+"_002"
            return new_job
        
        
    def update_product(self,jobID_list_by_product,current_product_id,current_job_sequence):
        product_db = self.db["product_db"]
        for key in jobID_list_by_product:
            product_db[key] = SplitJob.copy(self.db["product_db"][current_product_id])
            product_db[key]["_jobID_list"]=jobID_list_by_product[key]
            product_db[key]["product_name"]=product_db[key]["product_name"] + key[-4:]
            
            if key[-3:]!="000":
                product_db[key]["_process"] = product_db[key]["_process"][current_job_sequence-1:]
            else:
                product_db[key]["_process"] = product_db[key]["_process"][:current_job_sequence]
            product_db[key]["mother"]=current_product_id
            
        self.db["product_db"].pop(current_product_id,None)
            

    def split_job_by_key(self,jobID,num):
        job_db = self.db["job_db"]
        project_db = self.db["project_db"]
        product_db = self.db["product_db"]
        current_job_sequence = job_db[jobID]["job_sequence"]
        current_product_id = job_db[jobID]["product_id"]
        current_project_id = product_db[current_product_id]["project_id"]
        
        pending_job = {"0":[jobID]}
        tracked_job = {}
        new_seq = 0
        seq_increment_flag = False
        found_flag = False
        for seq,jid_list in product_db[current_product_id]["_jobID_list"].items():
            
            for jid in jid_list:
                if jid == "JID5":
                    print(seq, current_job_sequence)
                if jid == jobID:
                    found_flag = True
                if found_flag is False and (int(seq) < current_job_sequence):
                    
                    if seq not in tracked_job:
                        tracked_job[seq] = []
                    tracked_job[seq].append(jid)
                elif found_flag is False and (int(seq) == current_job_sequence):
                    
                    if seq not in tracked_job:
                        tracked_job[seq] = []
                    tracked_job[seq].append(jid)
                    
                elif found_flag is True and (int(seq) > current_job_sequence):
                    if new_seq == 0:
                        new_seq = 1
                    if str(new_seq) not in pending_job:
                        pending_job[str(new_seq)] = []
                    pending_job[str(new_seq)].append(jid)
                    seq_increment_flag = True
                elif found_flag is True and (int(seq) == current_job_sequence) and jid != jobID:
                    pending_job[str(new_seq)].append(jid)
                    seq_increment_flag = True
            if seq_increment_flag == True:
                new_seq +=1
        #case1: 拆批
        
        jobID_list_by_product = {
            current_product_id+"_000":SplitJob.copy(tracked_job),
            current_product_id+"_001":{},
            current_product_id+"_002":{}
        }
        
        
        #case2: 改成preproduct
        
        for seq,jid_list in tracked_job.items():
            for jid in jid_list:
                job_db[jid]["product_id"]=current_product_id+"_000"
        
        job_db[jobID]["pre_product"] = current_product_id+"_000"
        # create son job
        ratio = None
        
        for index,seq in enumerate(pending_job):
            
            for i in pending_job[seq]:
                ratio, new_job = self.split(job=job_db[i],num=num,ratio=ratio,sequence=int(seq))
                
                new_jobID = i + "_001"
#                 print(new_job)
#                 print(new_jobID)
                job_db[new_jobID] = new_job
                job_db[new_jobID]["mother"] = {
                    "jid": i,
                    "prdid": current_product_id
                }
                key = job_db[new_jobID]["product_id"]
                if seq not in jobID_list_by_product[key]:
                    jobID_list_by_product[key][seq] = []
                jobID_list_by_product[key][seq].append(new_jobID)
                
       
        # create brother job
       
        for seq, jid_list in pending_job.items():
            for i in jid_list:
                new_job = self.split(job=job_db[i],num=num,divide=False,ratio=ratio,sequence=int(seq))
                new_jobID = i + "_002"
                job_db[new_jobID] = new_job
                job_db[new_jobID]["mother"] = {
                    "jid": i,
                    "prdid": current_product_id
                }
                
                key = job_db[new_jobID]["product_id"]

                
                if seq not in jobID_list_by_product[key]:
                    jobID_list_by_product[key][seq] = []
                jobID_list_by_product[key][seq].append(new_jobID)
                
        print(jobID_list_by_product)
        self.update_product(jobID_list_by_product,current_product_id,current_job_sequence)
        project_db[current_project_id]["_product"].remove(current_product_id)
        project_db[current_project_id]["_product"].extend(jobID_list_by_product)
        #print(project_db[current_project_id]["_product"])
        for seq, jid_list in pending_job.items():
            for jid in jid_list:
                self.db["job_db"].pop(jid,None)
        return self.db

def job_resource_mapping_dict(resource_db, job_name, skill=None):
    candidate_resource = []
    for resource in resource_db:
        if job_name in resource_db[resource]["job_name"]:
            if skill and resource_db[resource]["operator_skill"] >= skill:
                candidate_resource.append(resource)
            elif skill is None:
                candidate_resource.append(resource)
    return candidate_resource

import logging
class ExtendJob(object):
    '''
    Only sequence with single job can be extended
    '''
    
    def __init__(self,db):
        self.db = db
    
    @staticmethod
    def inherit_job(current_job, new_job):
        for key, values in current_job.items():
            if key not in new_job.keys():
                new_job[key]=values
                
    def find_process_id(self, process_name):
        for key in self.db["process_db"]:
            if self.db["process_db"][key]["process_name"] == process_name:
                return key
        print("process unfound")
    
    def extend_job_by_key(self,jobID):
        job_info = self.db["job_info"]
        job_db = self.db["job_db"]
        project_db = self.db["project_db"]
        product_db = self.db["product_db"]
        current_job_sequence = job_db[jobID]["job_sequence"]
        current_product_id = job_db[jobID]["product_id"]
        current_project_id = product_db[current_product_id]["project_id"]
        
        last_id = int(list(job_db.keys())[-1][3:])
        print(jobID,current_job_sequence)
        print(job_db[jobID]["job_name"])
        print(last_id)
        print(product_db[current_product_id])
        if len(product_db[current_product_id]["_jobID_list"][str(current_job_sequence)]) > 1:
            logging.info(product_db[current_product_id]["_jobID_list"][current_job_sequence])
            logging.warning("violate extend rule")
            sys.exit(1)
        new_sequence = []
        
        current_job_index = list(product_db[current_product_id]["_jobID_list"].keys()).index(str(current_job_sequence))
        
        for index, new_job in enumerate(job_info):
            #print(new_job["job_name"])
            candidate = job_resource_mapping_dict(self.db["resource_db"], new_job["job_name"])
            if len(candidate) == 0:
                print("WARNING, candidate resource not found")
            new_job["candidate_resource"] = candidate

            new_job["job_sequence"]=current_job_sequence + index 
            new_job["resource"] = []
            if index == 0:
                new_JID = jobID
            else:
                new_JID = "JID" + str(last_id + index)
            new_sequence.append((new_job["job_sequence"],new_JID))
            self.inherit_job(current_job=job_db[jobID],new_job=new_job)
            job_db[new_JID]=new_job
        
            print("candidate_resource",new_job["candidate_resource"])
        print(new_sequence)
        
        brother_sequence_pair = []
        
        for seq,jid_list in product_db[current_product_id]["_jobID_list"].items():
            jid = jid_list[0]
            if int(seq) < current_job_sequence:
                continue
            elif int(seq) > current_job_sequence:
                job_db[jid]["job_sequence"] += len(new_sequence)-1
                brother_sequence_pair.append((job_db[jid]["job_sequence"], jid))
        
        
        for seq,JID in new_sequence+brother_sequence_pair:
            product_db[current_product_id]["_jobID_list"][str(seq)] = [JID]
            
        
        product_db[current_product_id]["_jobID_list"].pop(current_job_sequence,None)
        
        #print(job_db[jobID]["job_name"])
        prev = product_db[current_product_id]["_process"][:current_job_index]
        #print(prev)
        later = product_db[current_product_id]["_process"][current_job_index+1:]
        
        for seq,JID in new_sequence:
            prev.append(self.find_process_id(job_db[JID]["process_name"]))
        #print(prev)
        product_db[current_product_id]["_process"] = prev + later   
        self.db.pop('job_info',None)

        return self.db

class AddProduct(object):
    def __init__(self,db):
        self.db = db
    
    @staticmethod
    def inherit_product(current_product, new_product):
        for key, values in current_job.items():
            if key not in new_product.keys():
                new_job[key]=values
                
    @staticmethod
    def copy(old_job):
        new_job = {}
        for key, values in old_job.items():
            new_job[key]=values
        return new_job
    
    def find_process_id(self, process_name):
        for key in self.db["process_db"]:
            if self.db["process_db"][key]["process_name"] == process_name:
                return key
        print("process unfound")
    
    def add_job_by_key(self,jobID):
        product_list = self.db["product_list"]
        job_db = self.db["job_db"]
        project_db = self.db["project_db"]
        product_db = self.db["product_db"]
        current_job_sequence = job_db[jobID]["job_sequence"]
        current_product_id = job_db[jobID]["product_id"]
        current_project_id = product_db[current_product_id]["project_id"]
        
        last_id = int(list(job_db.keys())[-1][3:])
        print(jobID,current_job_sequence)
        print(job_db[jobID]["job_name"])
        print(last_id)
        print(product_db[current_product_id])
        print("==")
        new_sequence = []
        
        current_job_index = list(product_db[current_product_id]["_jobID_list"].keys()).index(str(current_job_sequence))
        product_db[current_product_id]["_jobID_list"][str(current_job_sequence)].remove(jobID)
        
        for index, new_job_name in enumerate(product_list):
            if index == 0:
                new_JID = jobID
            else:
                new_JID = "JID" + str(last_id + index)
        
            new_job = self.copy(job_db[jobID])
            new_job["subjob_name"] = new_job_name
            new_job["resource"] = []
            new_job["qty"] = job_db[jobID]["qty"]/len(product_list)
            job_db[new_JID]=new_job
            product_db[current_product_id]["_jobID_list"][str(current_job_sequence)].append(new_JID)
        
        #product_db[current_product_id]["_jobID_list"][str(current_job_sequence)].remove(jobID)
        new_process = [self.find_process_id(job_db[jobID]["process_name"])]*(len(product_list))
        #print(job_db[jobID]["job_name"])
        prev = product_db[current_product_id]["_process"][:current_job_index]
        #print(prev)
        later = product_db[current_product_id]["_process"][current_job_index+1:]
        prev.extend(new_process)
        product_db[current_product_id]["_process"] = prev + later
        print(product_db[current_product_id])
        print(len(product_db[current_product_id]["_jobID_list"][str(current_job_sequence)]))
        print(product_db[current_product_id]["_process"].count(self.find_process_id(job_db[jobID]["process_name"])))
        self.db.pop('product_list',None)
        return self.db