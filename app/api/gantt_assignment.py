# author: Joy
# Updated On 2021-01-04
import itertools

def default_schedule_config():
    cfg = {
        "percentDone":0,
        "startDate":"",
        "endDate":"",
        "expanded":True,
        "children":[]
    }
    return cfg
def default_expanded_config():
    cfg = {
        "percentDone":0,
        "startDate":"",
        "endDate":"",
        "expanded":True,
        "rollup":True,
        "children":[],
        "DeadlineDate":""
    }
    return cfg

FormattingGantt_db = {}
import sys

class FormattingGantt(object):
    id_iter = itertools.count()
    
    def __init__(self,filename):
        self.sol = {
            "success":True,
            "tasks":{
                "rows":[]
            },
            "dependencies":{
                "rows":[]
            },
            "resources":{
                "rows":[]
            },
            "assignments":{
                "rows":[]
            }    
        }
        self.schedule(filename)
        self.rows_id = []
    def schedule(self,filename):
        
        schedule = {
            "id":10000 + next(FormattingGantt.id_iter),
            "name":filename
        }
        schedule.update(default_schedule_config())
        #FormattingGantt.rows_id.append(schedule["id"])
        self.sol["tasks"]["rows"].insert(0,schedule)
        FormattingGantt_db[schedule["id"]] = self

    @staticmethod
    def get_intid(string):
        import re
        if "_" in string:
            match = re.match(r"([a-z]+)([0-9]+)_([0-9]+)", string, re.I)
        else:
            match = re.match(r"([a-z]+)([0-9]+)", string, re.I)
        
        if match:
            items = match.groups()
            
        if len(items) > 2:
            return int(str(int(items[1])+1) + items[2])
        return int(items[1])
    

    def check_duplicate(self, task_id):
        if task_id in self.rows_id:
            print("{} DUPLICATE!!!!".format(task_id))
            sys.exit(1)
        else:
            self.rows_id.append(task_id)
    
    def create_resource(self, db):
        scheduled_resource = db["schedule"]["res"]
        resource_db = db["resource_db"]
        resources = {
            'rows':[]
        }
        for r in scheduled_resource:
            rs = {
                "id":self.get_intid(r),
                "name":resource_db[r]["resource_name"]
            }

            resources["rows"].append(rs)
        
        self.sol["resources"].update(resources)
    
            
    def assign(self, db, wip_df=None):
        project_db = db["project_db"]
        product_db = db["product_db"]
        job_db = db["job_db"]

        project_schedule = {}
        product_schedule = {}
        resource_iter = 0
        dep_iter = 0
        assignments={
            'rows':[]
        }

        dependencies={
            'rows':[]
        }
        
        
        for PRJID in project_db:
            p = {
                "id": 100000 + self.get_intid(PRJID),
                "name":project_db[PRJID]["project_name"]  
            }
            project_schedule[PRJID] = p
            self.check_duplicate(p["id"])
            project_schedule[PRJID].update(default_expanded_config())
            
            pa_seq = 0
            
            for PRDID in project_db[PRJID]["_product"]:
                
                pa = {
                    "id": 1000000 + self.get_intid(PRDID),
                    "name":product_db[PRDID]["product_name"]
                }
                pa["deadlineDate"] = project_db[PRJID]["due"]
                product_schedule[PRDID] = pa
                self.check_duplicate(pa["id"])
                product_schedule[PRDID].update(default_expanded_config())
                
                sorted_dict = [(seq,product_db[PRDID]["_jobID_list"][seq]) for seq in sorted(product_db[PRDID]["_jobID_list"],key=int)]
                sorted_dict_count = 0
                fail_job = []
                prev_dep_js = []
                if len(sorted_dict) > 0:
                    for seq, JID_list in sorted_dict:
                        
                        for JID in JID_list:

                            if JID == "JID330":
                                print(job_db[JID])
                            if job_db[JID]["plan_start_time"] == None:
                                fail_job.append(JID)   
                                continue
                            if seq == '1' or sorted_dict_count == 0:
                                #print("defined product_start_time")
                                product_start_time = job_db[JID]["plan_start_time"].replace(" ", "T")
                                sorted_dict_count +=1 

                            if pa_seq == 0:
                                #print("defined project_start_time")
                                project_start_time = job_db[JID]["plan_start_time"].replace(" ", "T")
                            
                            js = {
                            "id":10000000 + self.get_intid(JID),
                            "name":"[" + pa["name"] + "-"+"{:04d}".format(job_db[JID]["job_sequence"])+"]" + job_db[JID]["job_name"],
                            "startDate":job_db[JID]["plan_start_time"].replace(" ", "T"),
                            "endDate":job_db[JID]["plan_end_time"].replace(" ", "T")
                            }
                            
                            js["rollup"]=True
                            js["color"]="teal"
                            js["constraintType"] = "startnoearlierthan"
                            # js["manuallyScheduled"]=True
                            #js["deadlineDate"] = project_db[PRJID]["due"].strftime('%Y-%m-%dT%H:%M:%S')
                            
                                 

                            self.check_duplicate(js["id"])

                            for r in job_db[JID]["resource"]:
                                assign = {
                                "id": resource_iter,
                                "event":js["id"],
                                "resource":self.get_intid(r)
                                }
                                assignments["rows"].append(assign)
                                
                                resource_iter +=1
                            current_sequence = job_db[JID]["job_sequence"]

                            # dependency 有兩種情況應該考慮: 
                            # (1) depend on 前置製品 
                            # (2) depend on job sequence
                            # (2.1) job sequence 是 1:1 
                            # (2.2) job sequence 是 parallel
                            
                            # (1) 
                            if (wip_df is not None) and (JID in wip_df.current_loc.values):
                                print("dependency with wip")
                                import ast
                                dep_list = ast.literal_eval(wip_df[wip_df.current_loc==JID].dep.values[0])
                                
                                for d in dep_list:
                                    dep_jid = wip_df.loc[d]["current_loc"]
                                    dep = {
                                        "id":dep_iter,
                                        "fromTask":10000000 + self.get_intid(dep_jid),
                                        "toTask":js["id"]}

                                    dependencies["rows"].append(dep)
                                    dep_iter +=1
                                
                            # (2.1)
                            elif current_sequence > 1 and str(current_sequence-1) in product_db[PRDID]["_jobID_list"] \
                                and len(product_db[PRDID]["_jobID_list"][str(current_sequence-1)]) == 1:
                                
                                dep = {
                                    "id": dep_iter,
                                    "fromTask":10000000 + self.get_intid(product_db[PRDID]["_jobID_list"][str(current_sequence-1)][0]), #僅1對1的情況
                                    "toTask":js["id"]}

                                dependencies["rows"].append(dep)
                                dep_iter+=1
                                #print("dependency job sequence 1:1 current: {} from task:{}".format(js["id"],self.get_intid(product_db[PRDID]["_jobID_list"][str(current_sequence-1)][0]) ) )
                            
                            # (2.2)
                            elif current_sequence > 1 and str(current_sequence-1) in product_db[PRDID]["_jobID_list"]  and len(product_db[PRDID]["_jobID_list"][str(current_sequence-1)]) > 1:
                                for prev in prev_dep_js:
                                    dep = {
                                        "id": dep_iter,
                                        "fromTask": prev["id"],
                                        "toTask": js["id"]
                                    }

                                    dependencies["rows"].append(dep)
                                    dep_iter +=1
                                prev_dep_js = []
                                #print("1:n dependency method incomplete")
                                
                            pa_seq +=1
                            product_schedule[PRDID]["children"].append(js)
                            # 1:n dependency 紀錄seq最後工作id
                            if len(product_db[PRDID]["_jobID_list"][str(current_sequence)]) > 1:
                                prev_dep_js.append(js)
                        
                #TODO: should change to lastest end time
                if job_db[JID]["plan_end_time"] != None:
                    
                    product_end_time = job_db[JID]["plan_end_time"].replace(" ", "T") 
                    product_schedule[PRDID]["startDate"]=product_start_time
                    product_schedule[PRDID]["endDate"]=product_end_time
                    #product_schedule[PRDID]["deadlineDate"]=project_db[PRJID]["due"].strftime('%Y-%m-%dT%H:%M:%S')
                    project_schedule[PRJID]["children"].append(product_schedule[PRDID])
            
            if len(project_db[PRJID]["_product"])>0 and len(sorted_dict) > 0:
                project_schedule[PRJID]["startDate"] = project_start_time 
                try:
                    project_schedule[PRJID]["endDate"] = product_end_time
                except:
                    project_schedule[PRJID]["endDate"] = None
                
            #project_schedule[PRJID]["DeadlineDate"] = project_db[PRJID]["due"].strftime('%Y-%m-%dT%H:%M:%S')
            self.sol["tasks"]["rows"][0]["children"].append(project_schedule[PRJID])
            

        self.sol["assignments"].update(assignments)
        self.sol["dependencies"].update(dependencies)
        self.create_resource(db)
        print(fail_job)
        print(self.rows_id)

        return self.sol
        
                    
                    
                    


        