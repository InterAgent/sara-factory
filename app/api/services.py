# author: Joy
# Updated On 2021-01-27 6:42

from flask_restful import Resource, request
from flask import jsonify, current_app, abort
from sqlalchemy import desc
from sqlalchemy import or_, and_


from app.api.features import SplitJob, ExtendJob, AddProduct
from app.api.features import update_db

from app.api.utils import save_json_to_file, get_current_time_string
from app.base.models import SQLScheduleDB
from app import db as sql_db

def get_latest_schedule():
    res = SQLScheduleDB.query.filter(SQLScheduleDB.status=="released").\
            order_by(desc(SQLScheduleDB.time)).first()
    return res

class Split(Resource):

    def report_split(self, jid, num):
        info = {
            "jid":jid,
            "rid": None,
            "start_time": get_current_time_string(),
            "end_time": None,
            "type": "scheduler",
            "status": "report_sent",
            "event_type":"工作拆批:{}".format(num) 
        } 
        from app.agents import dataAgent
        dataAgent.create_notification(info)

    @staticmethod
    def save_to_scheduler_db(res):
        count = SQLScheduleDB.query.count()
        index = "tmp" + str(count)
        
        gs = get_current_time_string()
        db_schedule = SQLScheduleDB(index=index, time=gs, schedule_result= res, schedule_status="releasedSplit", 
                      gantt_result=None,gantt_status=None, status="pending")
        sql_db.session.add(db_schedule)
        sql_db.session.commit()
    
    def get(self):
        return {
            'message': 'Hello World from Split!'
        }, 20

    def post(self, req):
        print("DEBUG: SPLIT, input format:{}".format(type(req)))
        req = req.split(':')
        jobID = req[0]
        num = int(req[1])

        schedule_row = get_latest_schedule()
        data = schedule_row.schedule_result
        
        SJ = SplitJob(data)
        res = SJ.split_job_by_key(jobID, num)
        self.save_to_scheduler_db(res)
        self.report_split(jobID, num)

        return 200

class Extend(Resource): #TODO:json
    def get(self):
        return {
            'message': 'Hello World from Extend'
        }, 20

    def post(self, jid):
        print("DEBUG: Extend")
        if not request.json:
            abort(400)
        data = request.json
        EJ = ExtendJob(data)
        res = EJ.extend_job_by_key(jid)
        
        ans = scheduler(res, project_key="project_db", process_key="process_db", 
                        job_key="job_db", resource_key="resource_db", product_key="product_db",
                        workingdays_key="workingdays_db", workinghours_key="workinghours_db", method='FIFO')
        filename = "schedule_" + get_current_time_string() + ".json"
        ans = update_db(db=res,schedule=ans,name=filename)
        save_json_to_file(data=ans, filename=filename)
        
        return ans
            
class Add(Resource): #TODO:json
    def get(self):
        return {
            'message': 'Hello World from Add'
        }, 20

    def post(self, jid):
        print("DEBUG: Add")
        if not request.json:
            abort(400)
        data = request.json
        ap = AddProduct(data)
        res = ap.add_job_by_key(jid)
        
        ans = scheduler(res, project_key="project_db", process_key="process_db", 
                        job_key="job_db", resource_key="resource_db", product_key="product_db",
                        workingdays_key="workingdays_db", workinghours_key="workinghours_db", method='FIFO')
        filename = "schedule_" + get_current_time_string() + ".json"
        ans = update_db(db=res,schedule=ans,name=filename)
        save_json_to_file(data=ans, filename=filename)
        
        return ans