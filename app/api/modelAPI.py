from flask_restful import Resource, request
from app.api.utils import serialize_db
from app.base.models import SQLNotificationModel, SQLJobModel, SQLResourceModel
from sqlalchemy import or_, and_
from app import db as sql_db

class Notification(Resource):
    @staticmethod
    def get():
        SQLNotificationEntities = SQLNotificationModel.query.filter(\
            or_(SQLNotificationModel.status=="report_sent",
            SQLNotificationModel.status=="resume_sent",
            )).all()
        sol = serialize_db(SQLNotificationEntities)
        for row in sol:
            if "job" in sol[row]["type"]:
                sql_j = SQLJobModel.query.filter(SQLJobModel.index == sol[row]["jid"]).first()
                sol[row]["name"] = sql_j.job_name + "-" + sql_j.index
            elif "resource" in sol[row]["type"]:
                sql_r = SQLResourceModel.query.filter(SQLResourceModel.index==sol[row]["rid"]).first()
                sol[row]["name"] = sql_r.resource_name
        return sol

    @staticmethod
    def post(info=None):
        if info is None:
            info = request.json

        count = SQLNotificationModel.query.count()
        sql_n = SQLNotificationModel(**info)
        sql_n.index = "NOTIF"+str(count)
        sql_db.session.add(sql_n)
        sql_db.session.commit()
        return sql_n.index

    @staticmethod
    def put(nid,info=None):
        if info is None:
            info = request.json
        sql_n = SQLNotificationModel.query.filter(SQLNotificationModel.index == nid).first()
        sql_n.status = "resume_sent"
        sql_n.end_time = info["end_time"]
        sql_db.session.commit()   

class UpdateJob(Resource):
    @staticmethod
    def put(args):
        from app.agents.jobAgent import JobAgent

        jid, method = args.split(':')
        
        ja = JobAgent(jid)
        if method == "start":
            ja.start()
            return 200
        elif method == "end":
            qty_update = request.json
            ja.end(qty_update)
            return 200
        elif method == "pause":
            qty_update = request.jso
            ja.pause(qty_update)
            return 200
        elif method == "report":
            info = request.json
            ja.report_abnormal(info)
            return 200
        elif method == "resume":
            info = request.json
            ja.abnormal_resolve(info)
            return 200

        return 400