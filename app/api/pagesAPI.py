from flask_restful import Resource, request
from flask import jsonify, current_app, abort
from sqlalchemy import desc
from sqlalchemy import or_, and_
from sqlalchemy.orm.attributes import flag_modified
from datetime import datetime, timezone
from datetime import timedelta 

from app.api.utils import serialize_first_query, serialize_db, serialize_db_with_entities, AlchemyEncoder
from app.api.utils import convert_str_to_datetime, get_current_time, convert_datetime_to_str
from app.base.models import SQLProjectModel, SQLProductModel, SQLJobModel, SQLProcessModel, SQLSchedulerSetting, SQLMaterialModel, SQLJobLib
from app.base.models import SQLWorkingDays, SQLWorkingHours, SQLResourceModel, SQLScheduleDB, SQLNotificationModel, SQLErrorSetting

from app import db as sql_db

class GetInprogressJobs(Resource):    
    def get(self):
        tupleEntities = sql_db.session.\
            query(SQLJobModel, SQLJobLib).\
            join(SQLJobLib).\
            filter(SQLJobModel.status=="running").\
            order_by(SQLJobModel.project_id, SQLJobModel.real_start_time).all()
        
        table = AlchemyEncoder().default(tupleEntities,mode="time")
        from app.agents.jobAgent import JobAgent
        for jid in table:
            ja = JobAgent(jid)
            table[jid]["current_loc"] = ja.get_product_loc()
            table[jid]['lot_nbr'] = ja.get_product_lot()
            table[jid]['project_type'] = ja.get_project_type() 
            
        return table

class ErrorSetting(Resource):
    def get(self):
        return serialize_db(SQLErrorSetting.query.all())
    def post(self):
        info = request.json
        count = SQLErrorSetting.query.count()
        sql_e = SQLErrorSetting(
            error_type=info["error_type"],
            error_group=info["error_group"],
            error_code=info["error_code"]
        )
        sql_e.index = "E" + str(count)
        sql_db.session.add(sql_e)
        sql_db.session.commit()
        return 200
    def delete(self,eid):
        SQLErrorSetting.query.filter(SQLErrorSetting.index == eid).delete()
        sql_db.session.commit()

class WorkingHour(Resource):
    def get(self):
        return serialize_db(SQLWorkingHours.query.all())
    def delete(self,whid):
        SQLWorkingHours.query.filter(SQLWorkingHours.index == whid).delete()
        sql_db.session.commit()

    def post(self):
        info = request.json
        hours_name = info["hours_name"]
        count = SQLWorkingHours.query.count()
        working_start_time = convert_str_to_datetime(info["working_start_time"],'12')

        working_end_time = convert_str_to_datetime(info["working_end_time"],'12')

        sql_wh = SQLWorkingHours(
            index = "WHID" + str(count),
            hours_name = info["hours_name"],
            working_hours = [convert_datetime_to_str(working_start_time,'12to24')+ "-" + \
                convert_datetime_to_str(working_end_time,'12to24')]
        )

        if info["break_start_time"] != '':
            break_start_time = convert_str_to_datetime(info["break_start_time"],'12')
            break_end_time = convert_str_to_datetime(info["break_end_time"],'12')
            sql_wh.break_time = [ convert_datetime_to_str(break_start_time,'12to24') +"-" \
                + convert_datetime_to_str(break_end_time,'12to24')]


        sql_db.session.add(sql_wh)
        sql_db.session.commit()

    def put(self,whid):
        sql_wh = SQLWorkingHours.query.filter(SQLWorkingHours.index == whid).first()

        if info["working_start_time"] != '' or info["working_end_time"] != '':

            if info["working_start_time"] != '':
                working_start_time = convert_str_to_datetime(info["working_start_time"],'12')
                working_start_time = convert_datetime_to_str(working_start_time,"12to24")
            else:
                working_start_time = sql_wh.working_hours[0].split('-')[0]
            if info["working_end_time"] != '':
                working_end_time = convert_str_to_datetime(info["working_end_time"],'12')
                working_end_time = convert_datetime_to_str(working_end_time,"12to24")
            else:
                working_end_time = sql_wh.working_hours[0].split('-')[1]

            sql_wh.working_hours = [working_start_time + '-' + working_end_time]

        if info["break_start_time"] != '' and info["break_end_time"] != '':

            break_start_time = convert_str_to_datetime(info["break_start_time"],'12')
            break_start_time = convert_datetime_to_str(break_start_time,'12to24')
            break_end_time = convert_str_to_datetime(info["break_end_time"],'12')
            break_end_time = convert_datetime_to_str(break_end_time,'12to24')
            break_time = break_start_time + '-' + break_end_time
            sql_wh.break_time.append(break_time)
        sql_db.session.commit()


class WorkingDay(Resource):
    def get(self):
        sol = serialize_db(SQLWorkingDays.query.all())

        for wdid in sol:
            sol[wdid]["events"] = []
            for day in sol[wdid]["working_days"]:
                event = {
                    "title": "工作日",
                    "start": day[0:10],
                    "borderColor": "#04a9f5",
                    "backgroundColor": "#04a9f5",
                    "textColor": '#fff'
                }
                sol[wdid]["events"].append(event)
        return sol
    def post(self, calendar_name):
        if not request.json:
            abort(400)
        info = request.json

        sql_c = SQLWorkingDays.query.filter(SQLWorkingDays.calendar_name == calendar_name).first()
        if sql_c is None:
            index = SQLWorkingDays.query.count()
            sql_c = SQLWorkingDays(
                index = "WDID" + str(index),
                calendar_name = calendar_name,
                working_days = [ c["start"] for c in info]
            )
            sql_db.session.add(sql_c)
        else:
            sql_c.working_days = [ c["start"] for c in info]

        sql_db.session.commit()
        return 200


class GetProduct(Resource):

    @staticmethod
    def get(prdid=None):
        from app.agents.productAgent import ProductAgent
        if prdid is None:
            SQLProductEntities = SQLProductModel.query.all()
            info = serialize_db(SQLProductEntities)
        else:
            pa = ProductAgent(prdid)

            info = pa.get_product_info()
            product_table = pa.get_product_table()
            info["job_list"] = AlchemyEncoder().default(product_table,mode="time")

        return info

    def put(self,prdid):
        sql_product = SQLProductModel.query.filter(SQLProductModel.index == prdid).first()
        info = request.json
        for key, value in info.items():
            if hasattr(sql_product, key):
                setattr(sql_product, key, value)
        sql_db.session.commit()

    def post(self):
        info = request.json
        index = "PRDID" + str(SQLProductModel.query.count())
        sql_product = SQLProductModel(**info)
        sql_product.index = index
        sql_project = SQLProjectModel.query.filter(SQLProjectModel.index == info["project_id"]).first()
        sql_project._product.append(index)
        sql_product.project_name = sql_project.project_name
        sql_product.product_full_name = sql_project.project_name + "_"+ sql_product.product_name
        if sql_product.lot_nbr != None or sql_product.lot_nbr != "":
            sql_product.product_full_name = sql_product.product_full_name + "_" + sql_product.lot_nbr

        flag_modified(sql_project, "_product")
        sql_db.session.add(sql_product)
        sql_db.session.commit()

    def delete(self,prdid):
        sql_product = SQLProductModel.query.filter(SQLProductModel.index == prdid).first()
        project_id = sql_product.project_id
        sql_p = SQLProjectModel.query.filter(SQLProjectModel.inde == project_id).first()
        sql_p.remove(sql_product.index)
        flag_modified(sql_project, "_product")
        sql_product.delete()
        sql_db.session.commit()

class EditProduct(Resource):

    def post(self):
        data = request.json
        from app.agents.jobAgent import GetJob
        for row in data:
            GetJob.post(info=row)

class ManageJobs(Resource):
    def get(self):
        data = {}
        SQLJobLibEntities = SQLJobLib.query.all()
        for sql_jlb in SQLJobLibEntities:
            candidate_resources = sql_jlb.primary_resource
            if candidate_resources != None:
                candidate_resources.update(sql_jlb.secondary_resource)
            data[sql_jlb.jlb_name] = [sql_jlb.jlb_name, sql_jlb.process_name, sql_jlb.sourcing, candidate_resources]
        return data

class ManageResources(Resource):

    def get(self):
        from app.agents.resourceAgent import ResourceAgent
        sol = {}
        SQLResourceEntities = SQLResourceModel.query.with_entities(
            SQLResourceModel.index, SQLResourceModel.resource_type).filter(
                or_(SQLResourceModel.resource_type != "vendor",
                    SQLResourceModel.resource_type == None)
            ).all()
        for sql_r in SQLResourceEntities:
            ra = ResourceAgent(sql_r.index)
            sol[sql_r.index] = ra.get_resource_info()
            wh, _ = ra.get_working_hours()
            sol[sql_r.index]["workinghours"] = (ra.sql_r.workingHours.hours_name,ra.sql_r.workingHours.working_hours)
            sol[sql_r.index]["process"] = ra.get_process()
            sol[sql_r.index].update(ra.get_setting_info())
        return sol

    def post(self,method):
        from app.agents.resourceAgent import ResourceAgent

        info = request.json
        rid = info['rid']
        ra = ResourceAgent(rid)

        if method == 'add_day_off':
            ra.add_day_off(info['day_off_list'])
        if method == "delete_day_off":
            ra.delete_day_off(info['delete_day_off_list'])
        if method == "add_day_on":
            ra.add_day_on(info['day_on_list'])
        if method =="delete_day_on":
            ra.delete_day_on(info['delete_day_on_list'])
        if method == "edit_lock_time":
            ra.update_lock_time(info['lock_time'])
        if method == "allow_overlapped_setting":
            ra.update_allow_overlap()
        return 200

class GetMaterial(Resource):
    def get(self):
        material_db = serialize_db(SQLMaterialModel.query.all())
        return material_db

class GetOutSourcing(Resource):
    def out_sourcing_job(self):
        res = sql_db.session.query(SQLJobModel, SQLJobLib).join(SQLJobLib).filter(
            and_(
                SQLJobModel.jlb_id == SQLJobLib.id,
                SQLJobLib.sourcing == "out-sourcing",
                SQLJobModel.status != "remove"
            )
        ).order_by(SQLJobModel.plan_start_time).all()
        return res

    def out_sourcing_resource(self):
        from app.agents.resourceAgent import ResourceAgent
        sol = {}
        SQLResourceEntities = SQLResourceModel.query.with_entities(
            SQLResourceModel.index, SQLResourceModel.resource_type
             ).filter(
                 SQLResourceModel.resource_type == "vendor"
             ).all()

        for sql_r in SQLResourceEntities:
            ra = ResourceAgent(sql_r.index)
            sol[sql_r.index] = ra.get_resource_info()
            sol[sql_r.index]["process"] = ra.get_process()
            sol[sql_r.index].update(ra.get_setting_info())

        return sol

    def get(self):

        SQLJobEntities = self.out_sourcing_job()
        sol = self.out_sourcing_resource()
        data = AlchemyEncoder().default(SQLJobEntities,mode="time")

        return [data, sol]

class GetResource(Resource):
    @staticmethod
    def get_resource_error_table():
        SQLErrorEntities = SQLErrorSetting.query.filter(SQLErrorSetting.error_type=='資源').all()
        #error_json = serialize_db(SQLErrorEntities)
        sol = {}

        for sql_e in SQLErrorEntities:
            if sql_e.error_group not in sol:
                sol[sql_e.error_group] = {}
            sol[sql_e.error_group][sql_e.index] = sql_e.error_code
        return sol

    def get(self,rid):
        error_table = self.get_resource_error_table()
        from app.agents.resourceAgent import ResourceAgent
        from app.agents.jobAgent import JobAgent
        ra = ResourceAgent(rid)
        sol = ra.get_resource_info()
        sol["jid_list"] = AlchemyEncoder().default(ra.get_job_table(),mode="time")
        # BRUTE:
        for jid in sol['jid_list']:
            ja = JobAgent(jid)
            sol['jid_list'][jid]['current_loc'] = ja.get_product_loc()
            sol['jid_list'][jid]['lot_nbr'] = ja.get_product_lot()
            sol['jid_list'][jid]['project_type'] = ja.get_project_type()
        ongoing_error = []

        for sql_n in SQLNotificationModel.query.filter(SQLNotificationModel.rid==rid).all():
            if sql_n.status == 'report_sent' or sql_n.status == 'report_received':
                ongoing_error.append(sql_n.event_type)
        sol["ongoing_error"] = ongoing_error
        return [sol, error_table]

    def post(self):
        info = request.json

        if "calendar_name" in info:
            sql_wd = SQLWorkingDays.query.filter(SQLWorkingDays.calendar_name == info["calendar_name"]).first()
        elif "workingdays" in info:
            sql_wd = SQLWorkingDays.query.filter(SQLWorkingDays.index == info["workingdays"]).first()
        if "hours_name" in info:
            sql_wh = SQLWorkingHours.query.filter(SQLWorkingHours.hours_name == info["hours_name"]).first()
        elif "workinghours" in info:
            sql_wh = SQLWorkingHours.query.filter(SQLWorkingHours.index == info["workinghours"]).first()

        sql_r = SQLResourceModel(
            resource_name = info["resource_name"],
            working_day = sql_wd.index,
            working_hours = sql_wh.index,
            resource_type = info["resource_type"],
            virtual_resource = False
        )

        sql_r.index = "RCID" + str(SQLResourceModel.query.count())
        sql_r.workingDays = sql_wd
        sql_r.workingHours = sql_wh
        if "job_name" in info:
            from app.agents.resourceAgent import ResourceAgent
            ra = ResourceAgent(sql_r.index,sql_r)
            ra.add_new_ability(info["job_name"])
        sql_db.session.add(sql_r)
        sql_db.session.commit()

    def delete(self,rid):
        info = request.json
        rid = info["resource_id"]
        SQLResourceModel.query.filter(SQLResourceModel.index == rid).delete()
        sql_db.session.commit()

    def put(self,rid):
        sql_r = SQLResourceModel.query.filter(SQLResourceModel.index == rid).first()
        args = rid.split(':')
        if len(args) == 1:
            rid = rid
            method = None
        else:
            rid = args[0]
            method = args[1]

        info = request.json

        if method == "add_job":
            job_name = info["job_name"]
            from app.agents.resourceAgent import ResourceAgent
            ra = ResourceAgent(sql_r.index, sql_r)
            ra.add_new_ability(job_name=job_name)
            return 200

        elif method == "delete_job":
            job_name = info["job_name"]
            if job_name in sql_r.job_name:
                sql_r.job_name.remove(job_name)
            return 200

        elif method == "report":
            from app.agents.resourceAgent import ResourceAgent
            ra = ResourceAgent(rid)
            ra.report_abnormal(info)
            return 200
        elif method == "resume":
            from app.agents.resourceAgent import ResourceAgent
            ra = ResourceAgent(rid)
            ra.abnormal_resolve(info)
            return 200

        if "calendar_name" in info:
                sql_wd = SQLWorkingDays.query.filter(SQLWorkingDays.calendar_name == info["calendar_name"]).first()
                sql_r.working_day = sql_wd.index
                sql_r.workingDays = sql_wd

        if "hours_name" in info:
            sql_wh = SQLWorkingHours.query.filter(SQLWorkingHours.hours_name == info["hours_name"]).first()
            sql_r.working_hours = sql_wh.index
            sql_r.workingHours = sql_wh

        if "allow_overlapped_capacity" in info:
            if info["allow_overlapped_capacity"] > 1:
                sql_r.allow_overlapped = True
            sql_r.capacity = info["allow_overlapped_capacity"]

        sql_db.session.commit()



class GetSchduleProgress(Resource):
    def get(self):
        sql_schedule = SQLScheduleDB.query.filter(
            SQLScheduleDB.schedule_status == "mes_schedule"
        ).order_by(desc(SQLScheduleDB.time)).first()
        if sql_schedule == None:
            return {}

        info = {
            "time": sql_schedule.time,
            "status": sql_schedule.status
        }
        return info


class ScheduleDetail(Resource):
    def get(self):
        from app.pages import ScheduleDetail as sd
        return sd.get_schedule_info()

class GetProject(Resource):
    def get(self,pid=None):
        if not pid:
            SQLProjectEntities = SQLProjectModel.query.all()

            project_db = serialize_db(SQLProjectEntities)
            #brute:
            for pid in project_db:
                earliest_order_date = None
                earliest_due = None

                project_db[pid]["product_list"] = {}
                for product_id in project_db[pid]["_product"]:
                    sql_product = SQLProductModel.query.filter(SQLProductModel.index == product_id).first()
                    project_db[pid]["product_list"][product_id] = sql_product.product_full_name

        else:
            from app.agents.projectAgent import ProjectAgent

            sql_p = SQLProjectModel.query.filter(SQLProjectModel.index==pid).first()
            pa = ProjectAgent(sql_p.index, sql_p)
            project_db = pa.get_general_info()
            SQLProductEntities = SQLProductModel.query.filter(SQLProductModel.project_id == pid).order_by(
                SQLProductModel.plan_end_time
            ).all()
            project_db["product_list"] = {}
            print(len(sql_p._product))
            print(len(SQLProductEntities))

            for sql_product in SQLProductEntities:
                project_db["product_list"][sql_product.index] = GetProduct.get(prdid=sql_product.index)

        return project_db
    def put(self,pid):
        sql_p = SQLProjectModel.query.filter(SQLProjectModel.index == pid).first()
        info = request.json
        for key, value in info.items():
            if hasattr(sql_p, key):
                setattr(sql_p, key, value)
        sql_db.session.commit()

        return {
            "message": "update success"
        }, 200
    def post(self):
        info = request.json
        index = "PID" + str(SQLProjectModel.query.count())

        sql_p = SQLProjectModel(**info)

        sql_p.index = index
        sql_db.session.add(sql_p)
        sql_db.session.commit()
        info["index"] = index

        return {
            "message": "create success",
            "data": info
        }, 200
class JobPerformance(Resource):
    def get(self):

        from app.agents import dataAgent
        sol = dataAgent.jobs_abnormal_table()

        return sol

class ResourcePerformance(Resource):

    def get(self):
        from app.agents.resourceAgent import ResourceAgent
        SQLResourceEntities = SQLResourceModel.query.all()
        sol = []
        for sql_r in SQLResourceEntities:
            rid = sql_r.index
            ra = ResourceAgent(rid, sql_r)
            info = ra.get_resource_info()
            working_info = ra.calculate_plan_working_hours_by_day()
            for date in working_info:
                sol.append([date, info["resource_name"],info["resource_type"],info["job_name"],\
                              working_info[date][0],working_info[date][1]])

        return sol

class Dashboard(Resource):
    def post(self,method):
        from app.pages import Dashboard as board
        return getattr(board,method)()

    def get(self):
        from app.pages import Dashboard as board
        sol = {}
        sol["total_project_count"] = board.project_count()
        sol["new_project_count"] = board.new_project_count()
        sol["scheduled_count"] = board.scheduled_project_count()
        sol["delay_count"] = board.delay_count()
        sol["inprogress_count_product"] = board.inprogress_count_product()
        sol["complete_goal"] = board.complete_goal_count_project()
        sol["goal_of_the_day"] = board.goal_of_the_day_count_project()
        sol["goal_of_the_day_job"] = board.goal_of_the_day_count()
        sol["complete_goal_job"] = board.complete_goal_count()
        sol["bottleneck"] = board.bottleneck_count()
        sol["job_abnormal"] = board.jobs_abnormals()
        sol["resource_abnormal"] = board.resource_abnormals()

        return sol

class ScheduleComparision(Resource):
    @staticmethod
    def get_end_job_json(db,pid):

        # 假設只有一個 product
        pid = db["project_db"][pid]["_product"][-1]
        last_sequence = list(db["product_db"][pid]["_jobID_list"].keys())[-1]
        last_jobID_list = db["product_db"][pid]["_jobID_list"][last_sequence]
        last_job = None
        latest_time = None
        for jid in last_jobID_list:
            if latest_time == None:
                latest_time = db["job_db"][jid]["plan_end_time"]
                last_job = jid
            else:
                if latest_time < db["job_db"][jid]["plan_end_time"]:
                    latest_time = db["job_db"][jid]["plan_end_time"]
                    last_job = jid

        return last_job


    def get(self):
        SQLScheduleEntities = SQLScheduleDB.query.filter(SQLScheduleDB.status!="New").all()
        SQLReleasedEntities = SQLScheduleDB.query.filter(SQLScheduleDB.status!="New").all()

        sol={
            "scheduled":[],
            "released":[]
        }
        for sql_s in SQLScheduleEntities:
            sol["scheduled"].append(sql_s.time)
        for sql_r in SQLReleasedEntities:
            sol["released"].append(sql_r.time)

        return sol

    def post(self,index_pair):
        scheduled_time = index_pair.split(',')[0]
        released_time = index_pair.split(',')[1]

        scheduled = SQLScheduleDB.query.filter(SQLScheduleDB.time==scheduled_time).first()
        released = SQLScheduleDB.query.filter(SQLScheduleDB.time==released_time).first()
        sol = {}
        for schedule_row in [scheduled, released]:
            if schedule_row == None:
                break
            delay = {}
            sol[schedule_row.time] = {
                "total":None,
                "delay":None,
                "delivery_rate":None,
                "scheduled_count": None
            }
            db = schedule_row.schedule_result
            total = len(db["project_db"])
            for pid in db["project_db"]:
                end_jobID = self.get_end_job_json(db,pid)

                if db["job_db"][end_jobID]["plan_end_time"] > db["project_db"][pid]["due"]:

                    start = convert_str_to_datetime(db["project_db"][pid]["due"])
                    end = convert_str_to_datetime(db["job_db"][end_jobID]["plan_end_time"])
                    diff = end - start

                    delay[pid] = {
                        "project_name": db["project_db"][pid]["project_name"],
                        "due": db["project_db"][pid]["due"],
                        "plan_end_time":db["job_db"][end_jobID]["plan_end_time"],
                        "delay_hour": str(diff),
                        "project_type": db["project_db"][pid]["project_type"]
                        }
            sol[schedule_row.time]["scheduled_count"] = total
            sol[schedule_row.time]["delay"] = delay
            sol[schedule_row.time]["delivery_rate"] = "{:.2f}".format(round(1 - len(delay)/total,2))
            sol[schedule_row.time]["scheduled_count"] = total
        return sol

class WorkCenterInfo(Resource):
    def get(self):
        from app.pages import WorkCenter as wc
        return wc.get_work_center_info()

from app.agents.dataAgent import find_bottleneck_threshold

class BottleneckAnalytics(Resource):

    @staticmethod
    def get_jlb_bottleneck_detail(jlb_name):
        #update resource schedule
        SQLJobLibEntities = sql_db.session.query(SQLJobModel, SQLJobLib).join(SQLJobLib).filter(
            SQLJobLib.jlb_name == jlb_name,
            SQLJobModel.bottleneck == True
        ).all()
        bottleneck_summary = {}
        for res_tuple in SQLJobLibEntities:
            sql_jlb = res_tuple[1]
            sql_j = res_tuple[0]
            warn, danger, ignore = find_bottleneck_threshold(jlb_name)
            if ignore is not None and ignore != "never":
                continue
            if sql_j.jlb.jlb_name not in bottleneck_summary:
                # 瓶頸分析
                candidate_resources = sql_j.jlb.primary_resource
                candidate_resources.update(sql_j.jlb.secondary_resource)
                #
                bottleneck_summary[sql_j.jlb.jlb_name] = {
                    "max_waiting_time": sql_j.waiting_time,
                    "jobs":[sql_j.jlb_id],
                    "projects": {sql_j.product_id:sql_j.product_name+sql_j.product.lot_nbr},
                    "candidate_resources": candidate_resources,
                    "resource_schedule": {}
                }
            else:
                if sql_j.waiting_time > bottleneck_summary[sql_j.jlb.jlb_name]["max_waiting_time"]:
                    bottleneck_summary[sql_j.jlb.jlb_name]["max_waiting_time"] = sql_j.waiting_time
                bottleneck_summary[sql_j.jlb.jlb_name]["jobs"].append(sql_j.index)
                bottleneck_summary[sql_j.jlb.jlb_name]["projects"][sql_j.product_id]=sql_j.product_name+sql_j.product.lot_nbr

        resourceHelper = GetResource()
        for jlb_name in bottleneck_summary:
            rid_list = [rid for sublist in bottleneck_summary[sql_j.jlb.jlb_name]["candidate_resources"].values() for rid in sublist]
            bottleneck_summary[sql_j.jlb.jlb_name]["resource_schedule"] = {}
            for rid in rid_list:
                resource_schedule = resourceHelper.get(rid)
                bottleneck_summary[sql_j.jlb.jlb_name]["resource_schedule"][rid] = resource_schedule[0]
        # ==== update color ==== #
        for jlb_name in bottleneck_summary:
            warn, danger, ignore = find_bottleneck_threshold(jlb_name)
            max_waiting_time = bottleneck_summary[jlb_name]["max_waiting_time"]
            if max_waiting_time >= warn and max_waiting_time < danger:
                bottleneck_summary[jlb_name]['color']  = "warning"
            elif max_waiting_time >= danger:
                bottleneck_summary[jlb_name]['color']  = "danger"
        return bottleneck_summary

    def get(self,jlb_name=None):
        if jlb_name:
            return self.get_jlb_bottleneck_detail(jlb_name)
        else:

            SQLJobEntities = SQLJobModel.query.filter(SQLJobModel.bottleneck == True).all()
            resourceHelper = GetResource()


            bottleneck_summary = {}
            for sql_j in SQLJobEntities:
                warn, danger, ignore = find_bottleneck_threshold(sql_j.jlb.jlb_name)
                if ignore is not None and ignore != "never":
                    continue
                if sql_j.jlb.jlb_name not in bottleneck_summary:
                    # 瓶頸分析
                    candidate_resources = sql_j.jlb.primary_resource
                    candidate_resources.update(sql_j.jlb.secondary_resource)
                    #
                    bottleneck_summary[sql_j.jlb.jlb_name] = {
                        "max_waiting_time": sql_j.waiting_time,
                        "jobs":[sql_j.jlb_id],
                        "projects": {sql_j.product_id:sql_j.product_name},
                        "candidate_resources": candidate_resources,
                        "resource_schedule": {}
                    }

                else:
                    if sql_j.waiting_time > bottleneck_summary[sql_j.jlb.jlb_name]["max_waiting_time"]:
                        bottleneck_summary[sql_j.jlb.jlb_name]["max_waiting_time"] = sql_j.waiting_time
                    bottleneck_summary[sql_j.jlb.jlb_name]["jobs"].append(sql_j.index)
                    bottleneck_summary[sql_j.jlb.jlb_name]["projects"][sql_j.product_id]=sql_j.product_name+sql_j.product.lot_nbr
            # ==== update color ==== #
            for jlb_name in bottleneck_summary:
                warn, danger, ignore = find_bottleneck_threshold(jlb_name)
                max_waiting_time = bottleneck_summary[jlb_name]["max_waiting_time"]
                if max_waiting_time >= warn and max_waiting_time < danger:
                    bottleneck_summary[jlb_name]['color']  = "warning"
                elif max_waiting_time >= danger:
                    bottleneck_summary[jlb_name]['color']  = "danger"

            return bottleneck_summary


class DelayOrder(Resource):
    @staticmethod
    def get_delay_product():

        SQLProductEntities = SQLProductModel.query.filter(
            SQLProductModel.plan_end_time > SQLProductModel.due).all()
        return SQLProductEntities

    @staticmethod
    def get_delay_detail(product_agent, detail=False):
        # dashboard
        t1 = convert_str_to_datetime(product_agent.sql_product.due,mode="day")
        t2 = convert_str_to_datetime(product_agent.sql_product.plan_end_time)
        delay_hours = t2 - t1
        res = product_agent.get_product_info()
        res["delay_hour"] = delay_hours.total_seconds()/3600
        res["waiting_time"] = product_agent.calculate_waiting_time()

        # ==== delay-color ==== #
        buffer_setting = 0.7
        if res["waiting_time"] == None:
            res['warning'] = "dark"
        else:
            if res["delay_hour"] > res["waiting_time"]:
                res['warning'] = "danger"
            elif res["delay_hour"] > res["waiting_time"]* buffer_setting:
                res['warning'] = "warning"
            else:
                res['warning'] = "dark"

        # ==== jlb info ==== #

        SQLJobEntities = product_agent.get_product_bottleneck_summary()
        res["bottleneck"] = list(set([res_tuple[0].jlb.process_name + "_" + res_tuple[0].jlb.jlb_name for res_tuple in SQLJobEntities]))

        if detail is False:
            return res

        # ==== jlb detail ==== #
        jlb_table = {}
        for res_tuple in SQLJobEntities:
            sql_j = res_tuple[0]
            sql_jlb = res_tuple[1]
            if sql_jlb.id in jlb_table:
                jlb_table[sql_jlb.id]["resource"].update(sql_j.resource)
            else:
                jlb_table[sql_jlb.id] = {}
                jlb_table[sql_jlb.id]["jlb_name"] = sql_jlb.jlb_name
                jlb_table[sql_jlb.id]["process_name"] = sql_jlb.process_name
                jlb_table[sql_jlb.id]["candidate_resource"] = sql_jlb.primary_resource
                jlb_table[sql_jlb.id]["candidate_resource"].update(sql_jlb.secondary_resource)
                jlb_table[sql_jlb.id]["resource"] = sql_j.resource
                jlb_table[sql_jlb.id].pop('primary_resource', None)
                jlb_table[sql_jlb.id].pop('secondary_resource', None)
        res["bottleneck_detail"] = jlb_table

        # ==== job detail ==== #
        job_table = AlchemyEncoder().default(product_agent.get_product_table(),mode="time")
        for jid in job_table:
            if job_table[jid]["plan_end_time"] is None:
                continue
            jid_plan_end_time = convert_str_to_datetime(job_table[jid]["plan_end_time"])
            job_table[jid]["buffer_time"] = (t1 - jid_plan_end_time).total_seconds()/3600
        res["job_table"] = job_table
        return res

    def get(self, product_id=None):
        from app.agents.productAgent import ProductAgent
        if product_id:
            sql_product = SQLProductModel.query.filter(SQLProductModel.index == product_id).first()
            product_agent = ProductAgent(sql_product.index, sql_product)
            return self.get_delay_detail(product_agent,detail=True)
        else:

            delay = {}
            SQLProductEntities = self.get_delay_product()
            for sql_product in SQLProductEntities:
                product_agent = ProductAgent(sql_product.index, sql_product)
                delay[sql_product.index] = self.get_delay_detail(product_agent)
            return delay

    def post(self,time):
        pass       
